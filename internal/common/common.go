package common

import (
	"math"
)

// Sqr returns the square of 'x'.
func Sqr(x float64) float64 {
	return x * x
}

// Trunc returns the integer value of 'x'.
func Trunc(x float64) int {
	return int(math.Trunc(x))
}

// Abs returns the absolute value of 'i'.
func Abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}
