package strandom

import (
	"math/rand"
)

// TStRandomMother implements the Interface interface
// using George Marsaglia's "TStRandomMother of All Random Number Generators".
type TStRandomMother struct {
	_FNminus4, _FNminus3, _FNminus2, _FNminus1, _FC uint32
	TStRandomBase
}

// NewTStRandomMother creates new TStRandomMother instance.
func NewTStRandomMother(seed uint32) TStRandomMother {
	m := TStRandomMother{}
	m.rsSetSeed(seed)
	m.TStRandomBase = TStRandomBase{m._Float64}
	return m
}

func (m *TStRandomMother) rsSetSeed(seed uint32) {
	if seed == 0 {
		seed = rand.Uint32()
	}
	m._FNminus4 = seed
	m._FNminus3 = uint32(69069 * uint64(m._FNminus4))
	m._FNminus2 = uint32(69069 * uint64(m._FNminus3))
	m._FNminus1 = uint32(69069 * uint64(m._FNminus2))
	m._FC = uint32(69069 * uint64(m._FNminus1))
}

const _TwoM31 float64 = 1.0 / 0x7FFFFFFF

func (m *TStRandomMother) _Float64() float64 {
	sum := uint64(m._FNminus4) * 2111111111
	m._FNminus4 = m._FNminus3
	sum += uint64(m._FNminus3) * 1492
	m._FNminus3 = m._FNminus2
	sum += uint64(m._FNminus2) * 1776
	m._FNminus2 = m._FNminus1
	sum += uint64(m._FNminus1) * 5115
	sum += uint64(m._FC)
	m._FNminus1 = uint32(sum)
	m._FC = uint32(sum >> 32)
	return float64(m._FNminus1>>1) * _TwoM31
}
