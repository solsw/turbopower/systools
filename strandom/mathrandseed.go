package strandom

import (
	"math/rand"
)

// MathRandSeed implements the Interface interface
// using 'seed'ed math/rand.Source and its Float64 function.
type MathRandSeed struct {
	TStRandomBase
}

// NewMathRandSeed creates new MathRandSeed instance.
func NewMathRandSeed(seed int64) MathRandSeed {
	s := rand.NewSource(seed)
	r := rand.New(s)
	return MathRandSeed{TStRandomBase{r.Float64}}
}
