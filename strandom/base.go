package strandom

import (
	"math"
)

// TStRandomBase implements the Interface interface.
type TStRandomBase struct {
	// asFloat generates a single uniformly distributed random number in the range (0.0 to 1.0).
	asFloat func() float64
}

// NewTStRandomBase creates new TStRandomBase instance.
// func NewTStRandomBase(asFloat func() float64) TStRandomBase {
// 	return TStRandomBase{asFloat}
// }

// AsBeta generates a single random number from the beta distribution in the range of (0.0 to 1.0).
func (b TStRandomBase) AsBeta(aShape1, aShape2 float64) float64 {
	var R1, R2 float64
	// if not ((aShape1 > 0.0) and (aShape2 > 0.0)) then
	//   raise EStPRNGError.Create(stscPRNGBetaShapeS);

	if aShape2 == 1.0 {
		for {
			R1 = b.asFloat()
			if R1 != 0.0 {
				break
			}
		}
		return math.Exp(math.Log(R1) / aShape1)
	} else if aShape1 == 1.0 {
		for {
			R1 = b.asFloat()
			if R1 != 0.0 {
				break
			}
		}
		return 1.0 - math.Exp(math.Log(R1)/aShape1)
	} else {
		R1 = b.AsGamma(aShape1, 1.0)
		R2 = b.AsGamma(aShape2, 1.0)
		return R1 / (R1 + R2)
	}
}

// AsCauchy generates a single random number from the standard Cauchy distribution.
func (b TStRandomBase) AsCauchy() float64 {
	var x, y float64
	for {
		for {
			x = b.asFloat()
			if x != 0.0 {
				break
			}
		}
		y = (b.asFloat() * 2.0) - 1.0
		if x*x+y*y < 1.0 {
			break
		}
	}
	return y / x
}

// AsChiSquared generates a single random number from the chi-squared distribution.
func (b TStRandomBase) AsChiSquared(aFreedom int) float64 {
	// if not (aFreedom > 0) then
	//   raise EStPRNGError.Create(stscPRNGDegFreedomS);

	return b.AsGamma(float64(aFreedom)*0.5, 2.0)
}

// AsErlang generates a single random number from the Erlang distribution.
func (b TStRandomBase) AsErlang(aMean float64, aOrder int) float64 {
	// if not (aMean > 0.0) then
	//   raise EStPRNGError.Create(stscPRNGMeanS);
	// if not (aOrder > 0) then
	//   raise EStPRNGError.Create(stscPRNGErlangOrderS);

	var Product float64
	if aOrder < 10 {
		Product = 1.0
		for i := 0; i < aOrder; i++ {
			Product = Product * b.asFloat()
		}
		return (-aMean) * math.Log(Product) / float64(aOrder)
	}
	return b.AsGamma(float64(aOrder), aMean)
}

// AsExponential generates a single random number from the exponential distribution.
func (b TStRandomBase) AsExponential(aMean float64) float64 {
	// if not (aMean > 0.0) then
	//   raise EStPRNGError.Create(stscPRNGMeanS);

	var R float64
	for {
		R = b.asFloat()
		if R != 0.0 {
			break
		}
	}
	return (-aMean) * math.Log(R)
}

// AsF generates a single random number from the F distribution.
func (b TStRandomBase) AsF(aFreedom1, aFreedom2 int) float64 {
	return (b.AsChiSquared(aFreedom1) * float64(aFreedom1)) / (b.AsChiSquared(aFreedom2) * float64(aFreedom2))
}

// AsFloat generates a single uniformly distributed random number in the range (0.0 to 1.0).
func (b TStRandomBase) AsFloat() float64 {
	return b.asFloat()
}

// AsGamma generates a single random number from the gamma distribution.
func (b TStRandomBase) AsGamma(aShape, aScale float64) float64 {
	// if not (aShape > 0.0) then
	//   raise EStPRNGError.Create(stscPRNGGammaShapeS);
	// if not (aScale > 0.0) then
	//   raise EStPRNGError.Create(stscPRNGGammaScaleS);

	var R float64
	// {there are three cases:
	//  ..0.0 < shape < 1.0, use Marsaglia's technique of
	//      Gamma(shape) = Gamma(shape+1) * uniform^(1/shape)}
	if aShape < 1.0 {
		for {
			R = b.asFloat()
			if R != 0.0 {
				break
			}
		}
		return aScale * b.rbMarsagliaGamma(aShape+1.0) * math.Exp(math.Log(R)/aShape)
	} else if aShape == 1.0 {
		// {..shape = 1.0: this is the same as exponential}
		for {
			R = b.asFloat()
			if R != 0.0 {
				break
			}
		}
		return aScale * (-math.Log(R))
	} else {
		// {..shape > 1.0: use Marsaglia./Tsang algorithm}
		return aScale * b.rbMarsagliaGamma(aShape)
	}
}

// AsInt generates a single uniformly distributed random number between 0 and the limit.
func (b TStRandomBase) AsInt(aUpperLimit int) int {
	// if not (aUpperLimit > 0) then
	//   raise EStPRNGError.Create(stscPRNGLimitS);

	return int(math.Trunc(b.asFloat() * float64(aUpperLimit)))
}

// AsIntInRange generates a single random integer value between aLowerLimit and aUpperLimit-1, inclusive.
func (b TStRandomBase) AsIntInRange(aLowerLimit, aUpperLimit int) int {
	// if not (aLowerLimit < aUpperLimit) then
	//   raise EStPRNGError.Create(stscPRNGUpperLimitS);

	return int(math.Trunc(b.asFloat()*float64(aUpperLimit-aLowerLimit))) + aLowerLimit
}

// AsLogNormal generates a single random number from the lognormal distribution.
func (b TStRandomBase) AsLogNormal(aMean, aStdDev float64) float64 {
	return math.Exp(b.AsNormal(aMean, aStdDev))
}

// AsNormal generates a single random number from the normal distribution.
func (b TStRandomBase) AsNormal(aMean, aStdDev float64) float64 {
	// if not (aStdDev > 0.0) then
	//   raise EStPRNGError.Create(stscPRNGStdDevS);

	return (b.rbMontyPythonNormal() * aStdDev) + aMean

	// (*** alternative: The Box-Muller transformation
	// var
	//   R1, R2     float64;
	//   RadiusSqrd float64;
	// begin
	//   {get two random numbers that define a point in the unit circle}
	//   repeat
	//     R1 := (2.0 * aRandGen.asFloat) - 1.0;
	//     R2 := (2.0 * aRandGen.asFloat) - 1.0;
	//     RadiusSqrd := sqr(R1) + sqr(R2);
	//   until (RadiusSqrd < 1.0) and (RadiusSqrd > 0.0);
	//   {apply Box-Muller transformation}
	//   Result := (R1 * sqrt(-2.0 * ln(RadiusSqrd) / RadiusSqrd) * aStdDev)
	//             + aMean;
	//  ***)
}

// AsT generates a single random number from the Student's t distribution.
func (b TStRandomBase) AsT(aFreedom int) float64 {
	// if not (aFreedom > 0) then
	//   raise EStPRNGError.Create(stscPRNGDegFreedomS);

	return b.rbMontyPythonNormal() / math.Sqrt(b.AsChiSquared(aFreedom)/float64(aFreedom))
}

// AsWeibull generates a single random number from the Weibull distribution.
func (b TStRandomBase) AsWeibull(aShape, aScale float64) float64 {
	// if not (aShape > 0) then
	//   raise EStPRNGError.Create(stscPRNGWeibullShapeS);
	// if not (aScale > 0) then
	//   raise EStPRNGError.Create(stscPRNGWeibullScaleS);

	var R float64
	for {
		R = b.asFloat()
		if R != 0.0 {
			break
		}
	}
	return math.Exp(math.Log(-math.Log(R))/aShape) * aScale
}

func (b TStRandomBase) rbMarsagliaGamma(aShape float64) float64 {
	var (
		d, c, x, v, u float64
		Done          bool
	)
	// {Notes: implements the Marsaglia/Tsang method of generating random
	//         numbers belonging to the gamma distribution:

	//         Marsaglia & Tsang, "A Simple Method for Generating Gamma
	//         Variables", ACM Transactions on Mathematical Software,
	//         Vol. 26, No. 3, September 2000, Pages 363-372

	//         It is pointless to try and work out what's going on in this
	//         routine without reading this paper :-)
	//         }

	d = aShape - (1.0 / 3.0)
	c = 1.0 / math.Sqrt(9.0*d)
	Done = false
	for !Done {
		for {
			x = b.rbMontyPythonNormal()
			v = 1.0 + (c * x)
			if v > 0.0 {
				break
			}
		}
		v = v * v * v
		u = b.asFloat()
		Done = u < (1.0 - 0.0331*x*x*x*x)
		if !Done {
			Done = math.Log(u) < (0.5*x*x)+d*(1.0-v+math.Log(v))
		}
	}
	return d * v
}

func (b TStRandomBase) rbMontyPythonNormal() float64 {
	var x, y, v, NonZeroRandom float64
	// {Notes: implements the Monty Python method of generating random
	//         numbers belonging to the Normal (Gaussian) distribution:

	//         Marsaglia & Tsang, "The Monty Python Method for Generating
	//         Random Variables", ACM Transactions on Mathematical
	//         Software, Vol. 24, No. 3, September 1998, Pages 341-350

	//         It is pointless to try and work out what's going on in this
	//         routine without reading this paper :-)

	//         Some constants:
	//         a = sqrt(ln(4))
	//         b = sqrt(2 * pi)
	//         s = a / (b - a)
	//         }

	// {step 1: generate a random number x between +/- sqrt(2*Pi) and
	//          return it if its absolute value is less than sqrt(ln(4));
	//          note that this exit will happen about 47% of the time}
	x = ((b.asFloat() * 2.0) - 1.0) * _Root2Pi
	if math.Abs(x) < _RootLn4 {
		return x
	}

	// {step 2a: generate another random number y strictly between 0 and 1}
	for {
		y = b.asFloat()
		if y != 0.0 {
			break
		}
	}

	// {step 2b: the first quadratic pretest avoids ln() calculation
	//           calculate v = 2.8658 - |x| * (2.0213 - 0.3605 * |x|)
	//           return x if y < v}
	v = 2.8658 - math.Abs(x)*(2.0213-0.3605*math.Abs(x))
	if y < v {
		return x
	}

	// {step 2c: the second quadratic pretest again avoids ln() calculation
	//           return s * (b - x) if y > v + 0.0506}
	if y > v+0.0506 {
		if x > 0 {
			return _MPN_s * (_Root2Pi - x)
		}
		return -_MPN_s * (_Root2Pi + x)
	}

	// {step 2d: return x if y < f(x) or
	//             ln(y) < ln(2) - (0.5 * x * x) }
	if math.Log(y) < (math.Ln2 - (0.5 * x * x)) {
		return x
	}

	// {step 3: translate x to s * (b - x) and return it if y > g(x) or
	//            ln(1 + s - y) < ln(2 * s) - (0.5 * x * x) }
	if x > 0 {
		x = _MPN_s * (_Root2Pi - x)
	} else {
		x = -_MPN_s * (_Root2Pi + x)
	}
	if math.Log(_MPN_sPlus1-y) < (_Ln2MPN_s - (0.5 * x * x)) {
		return x
	}

	// {step 4: the iterative process}
	for {
		for {
			NonZeroRandom = b.asFloat()
			if NonZeroRandom != 0.0 {
				break
			}
		}
		x = -math.Log(NonZeroRandom) * _InvRoot2Pi
		for {
			NonZeroRandom = b.asFloat()
			if NonZeroRandom != 0.0 {
				break
			}
		}
		y = -math.Log(NonZeroRandom)
		if (y + y) > (x * x) {
			break
		}
	}
	if NonZeroRandom < 0.5 {
		return -(_Root2Pi + x)
	}
	return _Root2Pi + x
}
