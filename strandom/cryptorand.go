package strandom

import (
	"github.com/solsw/mathrandhelper"
)

// CryptoRand implements the Interface interface using crypto/rand.
type CryptoRand struct {
	TStRandomBase
}

// NewCryptoRand creates new CryptoRand instance.
func NewCryptoRand() CryptoRand {
	r := mathrandhelper.NewCryptoRand()
	return CryptoRand{TStRandomBase{r.Float64}}
}
