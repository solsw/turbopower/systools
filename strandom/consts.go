package strandom

import (
	"math"
)

var (
	_Root2Pi    = math.Sqrt(2 * math.Pi)
	_InvRoot2Pi = 1.0 / _Root2Pi
	_RootLn4    = math.Sqrt(math.Log(4.0))
	_MPN_s      = _RootLn4 / (_Root2Pi - _RootLn4)
	_Ln2MPN_s   = math.Log(2.0 * _MPN_s)
	_MPN_sPlus1 = _MPN_s + 1.0
)
