package strandom

// Interface defines the interface for a whole series of Pseudo-Random Number Generators.
type Interface interface {
	// {uniform distributions}
	// AsFloat generates a single uniformly distributed random number in the range (0.0 to 1.0).
	AsFloat() float64
	// AsInt generates a single uniformly distributed random number between 0 and the limit.
	AsInt(aUpperLimit int) int
	// AsIntInRange generates a single random integer value between aLowerLimit and aUpperLimit-1, inclusive.
	AsIntInRange(aLowerLimit int, aUpperLimit int) int
	// {continuous non-uniform distributions}
	// AsBeta generates a single random number from the beta distribution in the range of (0.0 to 1.0).
	AsBeta(aShape1, aShape2 float64) float64
	// AsCauchy generates a single random number from the standard Cauchy distribution.
	AsCauchy() float64
	// AsChiSquared generates a single random number from the chi-squared distribution.
	AsChiSquared(aFreedom int) float64
	// AsErlang generates a single random number from the Erlang distribution.
	AsErlang(aMean float64, aOrder int) float64
	// AsExponential generates a single random number from the exponential distribution.
	AsExponential(aMean float64) float64
	// AsF generates a single random number from the F distribution.
	AsF(aFreedom1 int, aFreedom2 int) float64
	// AsGamma generates a single random number from the gamma distribution.
	AsGamma(aShape float64, aScale float64) float64
	// AsLogNormal generates a single random number from the lognormal distribution.
	AsLogNormal(aMean float64, aStdDev float64) float64
	// AsNormal generates a single random number from the normal distribution.
	AsNormal(aMean float64, aStdDev float64) float64
	// AsT generates a single random number from the Student's t distribution.
	AsT(aFreedom int) float64
	// AsWeibull generates a single random number from the Weibull distribution.
	AsWeibull(aShape float64, aScale float64) float64
}

var (
	_ Interface = NewTStRandomCombined(623, 28)
	_ Interface = NewTStRandomMother(623)
	_ Interface = NewMathRand()
	_ Interface = NewMathRandSeed(623)
	_ Interface = NewCryptoRand()
)
