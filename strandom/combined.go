package strandom

// TStRandomCombined implements the Interface interface
type TStRandomCombined struct {
	TStRandomBase
}

const (
	m1                = 2147483563
	m2                = 2147483399
	a1                = 40014
	q1                = 53668 // {equals m1 div a1}
	r1                = 12211 // {equals m1 mod a1}
	a2                = 40692
	q2                = 52774 // {equals m2 div a2}
	r2                = 3791  // {equals m2 mod a2}
	oneOverM1 float64 = 1.0 / m1
)

// NewTStRandomCombined creates new TStRandomCombined instance.
func NewTStRandomCombined(seed1, seed2 int) TStRandomCombined {
	f := func() float64 {
		var k, Z int
		// {advance first PRNG}
		k = seed1 / q1
		seed1 := (a1 * (seed1 - (k * q1))) - (k * r1)
		if seed1 < 0 {
			seed1 += m1
		}
		// {advance second PRNG}
		k = seed2 / q2
		seed2 := (a2 * (seed2 - (k * q2))) - (k * r2)
		if seed2 < 0 {
			seed2 += m2
		}
		// {combine the two seeds}
		Z = seed1 - seed2
		if Z <= 0 {
			Z = Z + m1 - 1
		}
		return float64(Z) * oneOverM1
	}
	return TStRandomCombined{TStRandomBase{f}}
}
