package strandom

import (
	"math/rand"
)

// MathRand implements the Interface interface using math/rand.Float64 function.
type MathRand struct {
	TStRandomBase
}

// NewMathRand creates new MathRand instance.
func NewMathRand() MathRand {
	return MathRand{TStRandomBase{rand.Float64}}
}
