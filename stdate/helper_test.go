package stdate

import (
	"reflect"
	"testing"
	"time"

	"github.com/solsw/timehelper"
)

func TestTimeToTStDate(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		args args
		want TStDate
	}{
		{name: "0", args: args{t: timehelper.DateYMD(1600, time.January, 1)}, want: 0},
		{name: "1", args: args{t: timehelper.DateYMD(1601, time.January, 1)}, want: 366},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TimeToTStDate(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TimeToTStDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
