package stdate

import (
	"math"
	"time"

	"github.com/solsw/timehelper"
	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
)

// TimeToTStDate converts time.Time to TStDate.
func TimeToTStDate(t time.Time) TStDate {
	if t.Before(_TStDateStart) {
		return BadDate
	}
	return TStDate(timehelper.DayDiff(_TStDateStart, t.In(time.UTC)))
}

// TStDateToTime converts TStDate to time.Time.
func TStDateToTime(d TStDate) time.Time {
	return _TStDateStart.AddDate(0, 0, int(d))
}

// DurationToTStTime converts time.Duration to TStTime.
func DurationToTStTime(d time.Duration) TStTime {
	return TStTime(d.Seconds())
}

// TStTimeToDuration converts TStTime to time.Duration.
func TStTimeToDuration(t TStTime) time.Duration {
	return timehelper.MulSecond(float64(t))
}

// ResolveEpoch {-Convert 2-digit year to 4-digit year according to Epoch}
func ResolveEpoch(Year, Epoch int) int {
	if Year < 100 {
		EpochYear := Epoch % 100
		EpochCent := (Epoch / 100) * 100
		if Year < EpochYear {
			Year += EpochCent + 100
		} else {
			Year += EpochCent
		}
	}
	return Year
}

// ValidDate {-Verify that day, month, year is a valid date}
func ValidDate(Day, Month, Year, Epoch int) bool {
	Year = ResolveEpoch(Year, Epoch)
	err := timehelper.ValidDate(Year, time.Month(Month), Day)
	return err == nil
}

// AstJulianDate {-Returns the Astronomical Julian Date from a TStDate}
func AstJulianDate(Julian TStDate) float64 {
	// {Subtract 0.5d since Astronomical JD starts at noon
	// 	while TStDate (with implied .0) starts at midnight}
	return float64(Julian) - 0.5 + DeltaJD
}

// AstJulianDatePrim {-Returns an Astronomical Julian Date for any year, even those outside
// MinYear..MaxYear}
func AstJulianDatePrim(Year, Month, Date int, UT TStTime) float64 {
	var (
		A, B   int
		GC     bool
		Result float64
	)
	Result = float64(math.MinInt64)
	if err := timehelper.ValidDate(Year, time.Month(Month), Date); err != nil {
		return Result
	}
	if (UT < 0) || (UT >= SecondsInDay) {
		return Result
	}

	if Month <= 2 {
		Year = Year - 1
		Month = Month + 12
	}
	A = common.Abs(Year / 100)

	if Year > 1582 {
		GC = true
	} else if Year == 1582 {
		if Month > 10 {
			GC = true
		} else if Month < 10 {
			GC = false
		} else {
			if Date >= 15 {
				GC = true
			} else {
				GC = false
			}
		}
	} else {
		GC = false
	}
	if GC {
		B = 2 - A + common.Abs(A/4)
	} else {
		B = 0
	}

	return math.Trunc(365.25*(float64(Year)+4716)) + math.Trunc(30.6001*(float64(Month)+1)) + float64(Date) + float64(B) - 1524.5 + float64(UT)/SecondsInDay
}

// AstJulianDateToStDate {-Returns a TStDate from an Astronomical Julian Date.
//
//	Truncate TRUE   Converts to appropriate 0 hours then truncates
//					 FALSE  Converts to appropriate 0 hours, then rounds to
//									nearest;}
func AstJulianDateToStDate(AstJulian float64, Truncate bool) TStDate {
	// {Convert to TStDate, adding 0.5d for implied .0d of TStDate}
	AstJulian = AstJulian + 0.5 - DeltaJD
	if (AstJulian < float64(MinDate)) || (AstJulian > float64(MaxDate)) {
		return BadDate
	}
	if Truncate {
		return common.Trunc(AstJulian)
	}
	return common.Trunc(AstJulian + 0.5)
}

// StDateToDMY {-Convert from a julian date to month, day, year}
func StDateToDMY(Julian TStDate) (Day, Month, Year int) {
	if Julian == BadDate {
		return 0, 0, 0
	}
	t := TStDateToTime(Julian)
	return t.Day(), int(t.Month()), t.Year()
}

// DMYToStDate {-Convert from day, month, year to a julian date}
func DMYToStDate(Day, Month, Year, Epoch int) TStDate {
	Year = ResolveEpoch(Year, Epoch)
	if !ValidDate(Day, Month, Year, Epoch) {
		return BadDate
	}
	return TimeToTStDate(timehelper.DateYMD(Year, time.Month(Month), Day))
}
