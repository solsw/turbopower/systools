package stdate

type (
	// TStDate {In STDATE, dates are stored in long integer format as the number of days
	//         since January 1, 1600}
	TStDate = int

	// TStTime {STDATE handles time in a manner similar to dates, representing a given
	//         time of day as the number of seconds since midnight}
	TStTime = int

	// TStDateTimeRec {This record type simply combines the two basic date types defined by
	//                STDATE, Date and Time}
	TStDateTimeRec struct {
		D TStDate
		T TStTime
	}
)
