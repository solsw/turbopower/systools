package stdate

import (
	"math"
	"time"

	"github.com/solsw/timehelper"
)

const (
	// MinYear {Minimum valid year for a date variable}
	MinYear TStDate = 1600
	// MaxYear {Maximum valid year for a date variable}
	MaxYear TStDate = 3999
	// MinDate {Minimum valid date for a date variable - 01/01/1600}
	MinDate TStDate = 0x00000000
	// MaxDate {Maximum valid date for a date variable - 12/31/3999}
	MaxDate TStDate = 0x000D6025
	// BadDate {This value is used to represent an invalid date, such as 12/32/1992}
	BadDate TStDate = math.MinInt32

	// DeltaJD {Days between 1/1/-4173 and 1/1/1600}
	DeltaJD = 0x00232DA8

	// MinTime {Minimum valid time for a time variable - 00:00:00 am}
	MinTime TStTime = 0
	// MaxTime {Maximum valid time for a time variable - 23:59:59 pm}
	MaxTime TStTime = 86399
	// BadTime {This value is used to represent an invalid time of day, such as 12:61:00}
	BadTime TStTime = math.MinInt32

	// SecondsInDay {Number of seconds in a day}
	SecondsInDay = 86400
)

var _TStDateStart time.Time

func init() {
	_TStDateStart = timehelper.DateYMD(1600, time.January, 1)
}
