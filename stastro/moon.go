package stastro

import (
	"math"

	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

// MoonPosPrim {-computes J2000 coordinates of the moon}
func MoonPosPrim(UT stdate.TStDateTimeRec) TStMoonPosRec {
	var (
		JD, TD, JCent, JC2, JC3, JC4, LP, D, M, MP, F, I, A1, A2, A3,
		MoonLong, MoonLat, MoonDst, S1, C1, SunRA, SunDC, EE, EES float64
		SP     TStSunXYZRec
		Result TStMoonPosRec
	)
	JD = stdate.AstJulianDate(UT.D) + float64(UT.T)/86400
	JCent = (JD - 2451545) / 36525
	JC2 = common.Sqr(JCent)
	JC3 = JC2 * JCent
	JC4 = common.Sqr(JC2)

	SP = SunPosPrim(UT)
	SunRA = math.Atan2(SP.SunX, SP.SunY) * radcor
	SunDC = math.Asin(SP.SunZ/SP.RV) * radcor

	// {Lunar mean longitude}
	LP = 218.3164591 + (481267.88134236 * JCent) - (1.3268e-3 * JC2) + (JC3 / 538841) - (JC4 / 65194000)
	LP = Frac(LP/360) * 360
	if LP < 0 {
		LP = LP + 360
	}
	LP = LP / radcor

	// {Lunar mean elongation}
	D = 297.8502042 + (445267.1115168 * JCent) - (1.63e-3 * JC2) + (JC3 / 545868) - (JC4 / 113065000)
	D = Frac(D/360) * 360
	if D < 0 {
		D = D + 360
	}
	D = D / radcor

	// {Solar mean anomaly}
	M = 357.5291092 + (35999.0502909 * JCent) - (1.536e-4 * JC2) + (JC3 / 24490000)
	M = Frac(M/360) * 360
	if M < 0 {
		M = M + 360
	}
	M = M / radcor

	// {Lunar mean anomaly}
	MP = 134.9634114 + (477198.8676313 * JCent) + (8.997e-3 * JC2) + (JC3 / 69699) - (JC4 / 14712000)
	MP = Frac(MP/360) * 360
	if MP < 0 {
		MP = MP + 360
	}
	MP = MP / radcor

	// {Lunar argument of latitude}
	F = 93.2720993 + (483202.0175273 * JCent) - (3.4029e-3 * JC2) - (JC3 / 3526000) + (JC4 / 863310000)
	F = Frac(F/360) * 360
	if F < 0 {
		F = F + 360
	}
	F = F / radcor

	// {Other required arguments}
	A1 = 119.75 + (131.849 * JCent)
	A1 = Frac(A1/360) * 360
	if A1 < 0 {
		A1 = A1 + 360
	}
	A1 = A1 / radcor

	A2 = 53.09 + (479264.290 * JCent)
	A2 = Frac(A2/360) * 360
	if A2 < 0 {
		A2 = A2 + 360
	}
	A2 = A2 / radcor

	A3 = 313.45 + (481266.484 * JCent)
	A3 = Frac(A3/360) * 360
	if A3 < 0 {
		A3 = A3 + 360
	}
	A3 = A3 / radcor

	// {Earth's orbital eccentricity}
	EE = 1.0 - (2.516e-3 * JCent) - (7.4e-6 * JC2)
	EES = common.Sqr(EE)

	MoonLong = 6288774 * math.Sin(MP)
	MoonLong += 1274027 * math.Sin(2*D-MP)
	MoonLong += 658314 * math.Sin(2*D)
	MoonLong += 213618 * math.Sin(2*MP)
	MoonLong -= 185116 * math.Sin(M) * EE
	MoonLong -= 114332 * math.Sin(2*F)
	MoonLong += 58793 * math.Sin(2*(D-MP))
	MoonLong += 57066 * math.Sin(2*D-M-MP) * EE
	MoonLong += 53322 * math.Sin(2*D-MP)
	MoonLong += 45758 * math.Sin(2*D-M) * EE
	MoonLong -= 40923 * math.Sin(M-MP) * EE
	MoonLong -= 34720 * math.Sin(D)
	MoonLong -= 30383 * math.Sin(M+MP) * EE
	MoonLong += 15327 * math.Sin(2*(D-F))
	MoonLong -= 12528 * math.Sin(MP+2*F)
	MoonLong += 10980 * math.Sin(MP-2*F)
	MoonLong += 10675 * math.Sin(4*D-MP)
	MoonLong += 10034 * math.Sin(3*MP)
	MoonLong += 8548 * math.Sin(4*D-2*MP)
	MoonLong -= 7888 * math.Sin(2*D+M-MP) * EE
	MoonLong -= 6766 * math.Sin(2*D+M) * EE
	MoonLong -= 5163 * math.Sin(D-MP)
	MoonLong += 4987 * math.Sin(D+M) * EE
	MoonLong += 4036 * math.Sin(2*D-M+MP) * EE
	MoonLong += 3994 * math.Sin(2*(D+MP))
	MoonLong += 3861 * math.Sin(4*D)
	MoonLong += 3665 * math.Sin(2*D-3*MP)
	MoonLong -= 2689 * math.Sin(M-2*MP) * EE
	MoonLong -= 2602 * math.Sin(2*D-MP+2*F)
	MoonLong += 2390 * math.Sin(2*D-M-2*MP) * EE
	MoonLong -= 2348 * math.Sin(D-MP)
	MoonLong += 2236 * math.Sin(2*(D-M)) * EES
	MoonLong -= 2120 * math.Sin(M-2*MP) * EE
	MoonLong -= 2069 * math.Sin(2*M) * EE
	MoonLong += 2048 * math.Sin(2*D-2*M-MP) * EES
	MoonLong -= 1773 * math.Sin(2*D+MP-2*F)
	MoonLong -= 1595 * math.Sin(2*(D+F))
	MoonLong += 1215 * math.Sin(4*D-M-MP) * EE
	MoonLong -= 1110 * math.Sin(2*(MP+F))
	MoonLong -= 892 * math.Sin(3*D-MP)
	MoonLong -= 810 * math.Sin(2*D-M-MP) * EE
	MoonLong += 759 * math.Sin(4*D-M-2*MP) * EE
	MoonLong -= 713 * math.Sin(2*M-MP) * EE
	MoonLong -= 700 * math.Sin(2*D+2*M-MP) * EES
	MoonLong += 691 * math.Sin(2*D+M-2*MP) * EE
	MoonLong += 596 * math.Sin(2*D-M-2*F) * EE
	MoonLong += 549 * math.Sin(4*D+MP)
	MoonLong += 537 * math.Sin(4*MP)
	MoonLong += 520 * math.Sin(4*D-M) * EE

	MoonDst = -20905355 * math.Cos(MP)
	MoonDst -= 3699111 * math.Cos(2*D-MP)
	MoonDst -= 2955968 * math.Cos(2*D)
	MoonDst -= 569925 * math.Cos(2*MP)
	MoonDst += 48888 * math.Cos(M) * EE
	MoonDst -= 3149 * math.Cos(2*F)
	MoonDst += 246158 * math.Cos(2*(D-MP))
	MoonDst -= 152138 * math.Cos(2*D-M-MP) * EE
	MoonDst -= 170733 * math.Cos(2*D-MP)
	MoonDst -= 204586 * math.Cos(2*D-M) * EE
	MoonDst -= 129620 * math.Cos(M-MP) * EE
	MoonDst += 108743 * math.Cos(D)
	MoonDst += 104755 * math.Cos(M-MP) * EE
	MoonDst += 10321 * math.Cos(2*D-2*F)
	MoonDst += 79661 * math.Cos(MP-2*F)
	MoonDst -= 34782 * math.Cos(4*D-MP)
	MoonDst -= 23210 * math.Cos(3*MP)
	MoonDst -= 21636 * math.Cos(4*D-2*MP)
	MoonDst += 24208 * math.Cos(2*D+M-MP) * EE
	MoonDst += 30824 * math.Cos(2*D-M) * EE
	MoonDst -= 8379 * math.Cos(D-MP)
	MoonDst -= 16675 * math.Cos(D+M) * EE
	MoonDst -= 12831 * math.Cos(2*D-M+MP) * EE
	MoonDst -= 10445 * math.Cos(2*D+2*MP)
	MoonDst -= 11650 * math.Cos(4*D) * EE
	MoonDst += 14403 * math.Cos(2*D+3*MP)
	MoonDst -= 7003 * math.Cos(M-2*MP) * EE
	MoonDst += 10056 * math.Cos(2*D-M-2*MP) * EE
	MoonDst += 6322 * math.Cos(D+MP)
	MoonDst -= 9884 * math.Cos(2*D-2*M) * EES
	MoonDst += 5751 * math.Cos(M+2*MP) * EE
	MoonDst -= 4950 * math.Cos(2*D-2*M-MP) * EES
	MoonDst += 4130 * math.Cos(2*D+MP+2*F)
	MoonDst -= 3958 * math.Cos(4*D-M-MP) * EE
	MoonDst += 3258 * math.Cos(3*D-MP)
	MoonDst += 2616 * math.Cos(2*D+M+MP) * EE
	MoonDst -= 1897 * math.Cos(4*D-M-2*MP) * EE
	MoonDst -= 2117 * math.Cos(2*M-MP) * EES
	MoonDst += 2354 * math.Cos(2*D+2*M-MP) * EES
	MoonDst -= 1423 * math.Cos(4*D+MP)
	MoonDst -= 1117 * math.Cos(4*MP)
	MoonDst -= 1571 * math.Cos(4*D-M) * EE
	MoonDst -= 1739 * math.Cos(D-2*MP)
	MoonDst -= 4421 * math.Cos(2*MP-2*F)
	MoonDst += 1165 * math.Cos(2*M+MP)
	MoonDst += 8752 * math.Cos(2*D-MP-2*F)

	MoonLat = 5128122 * math.Sin(F)
	MoonLat += 280602 * math.Sin(MP+F)
	MoonLat += 277693 * math.Sin(MP-F)
	MoonLat += 173237 * math.Sin(2*D-F)
	MoonLat += 55413 * math.Sin(2*D-MP+F)
	MoonLat += 46271 * math.Sin(2*D-MP-F)
	MoonLat += 32573 * math.Sin(2*D+F)
	MoonLat += 17198 * math.Sin(2*MP+F)
	MoonLat += 9266 * math.Sin(2*D+MP-F)
	MoonLat += 8822 * math.Sin(2*MP-F)
	MoonLat += 8216 * math.Sin(2*D-M-F) * EE
	MoonLat += 4324 * math.Sin(2*D-2*MP-F)
	MoonLat += 4200 * math.Sin(2*D+MP+F)
	MoonLat -= 3359 * math.Sin(2*D+M-F) * EE
	MoonLat += 2463 * math.Sin(2*D-M-MP+F) * EE
	MoonLat += 2211 * math.Sin(2*D-M+F) * EE
	MoonLat += 2065 * math.Sin(2*D-M-MP-F) * EE
	MoonLat -= 1870 * math.Sin(M-MP-F) * EE
	MoonLat += 1828 * math.Sin(4*D-MP-F)
	MoonLat -= 1794 * math.Sin(M+F) * EE
	MoonLat -= 1749 * math.Sin(3*F)
	MoonLat -= 1565 * math.Sin(M-MP+F) * EE
	MoonLat -= 1491 * math.Sin(D+F)
	MoonLat -= 1475 * math.Sin(M+MP+F) * EE
	MoonLat -= 1410 * math.Sin(M+MP-F) * EE
	MoonLat -= 1344 * math.Sin(M-F) * EE
	MoonLat -= 1335 * math.Sin(D-F)
	MoonLat += 1107 * math.Sin(3*MP+F)
	MoonLat += 1021 * math.Sin(4*D-F)
	MoonLat += 833 * math.Sin(4*D-MP+F)
	MoonLat += 777 * math.Sin(MP-3*F)
	MoonLat += 671 * math.Sin(4*D-2*MP+F)
	MoonLat += 607 * math.Sin(2*D-3*F)
	MoonLat += 596 * math.Sin(2*D+2*MP-F)
	MoonLat += 491 * math.Sin(2*D-M+MP-F) * EE
	MoonLat -= 451 * math.Sin(2*D-2*MP+F)
	MoonLat += 439 * math.Sin(3*MP-F)
	MoonLat += 422 * math.Sin(2*D+2*MP+F)
	MoonLat += 421 * math.Sin(2*D-3*MP-F)
	MoonLat -= 366 * math.Sin(2*D+M-MP+F) * EE
	MoonLat -= 351 * math.Sin(2*D+M+F) * EE
	MoonLat += 331 * math.Sin(4*D+F)
	MoonLat += 315 * math.Sin(2*D-M+MP+F) * EE
	MoonLat += 302 * math.Sin(2*D-2*M-F) * EES
	MoonLat -= 283 * math.Sin(MP+3*F)
	MoonLat -= 229 * math.Sin(2*D+M+MP-F) * EE
	MoonLat += 223 * math.Sin(D+M-F) * EE
	MoonLat += 223 * math.Sin(D+M+F) * EE

	MoonLong = MoonLong + 3958*math.Sin(A1) + 1962*math.Sin(LP-F) + 318*math.Sin(A2)

	MoonLat -= 2235 * math.Sin(LP)
	MoonLat += 382 * math.Sin(A3)
	MoonLat += 175 * math.Sin(A1-F)
	MoonLat += 175 * math.Sin(A1+F)
	MoonLat += 127 * math.Sin(LP-MP)
	MoonLat -= 115 * math.Sin(LP+MP)

	MoonLong = LP + MoonLong/1000000/radcor
	MoonLat = MoonLat / 1000000 / radcor
	MoonDst = 385000.56 + MoonDst/1000

	Result.Plx = math.Asin(6378.14/MoonDst) * radcor
	Result.Dia = 358473400 / MoonDst * 2 / 3600

	S1 = math.Sin(MoonLong)*math.Cos(OB2000) - math.Tan(MoonLat)*math.Sin(OB2000)
	C1 = math.Cos(MoonLong)
	Result.RA = math.Atan2(C1, S1) * radcor

	TD = math.Sin(MoonLat)*math.Cos(OB2000) + math.Cos(MoonLat)*math.Sin(OB2000)*math.Sin(MoonLong)
	TD = math.Asin(TD)
	Result.DC = TD * radcor

	I = math.Sin(SunDC/radcor)*math.Sin(TD) + math.Cos(SunDC/radcor)*math.Cos(TD)*math.Cos((SunRA-Result.RA)/radcor)
	Result.Phase = (1 - I) / 2

	I = math.Acos(I) * radcor
	Result.Elong = (Result.RA - SunRA)
	if Result.Elong < 0 {
		Result.Elong = 360 + Result.Elong
	}
	if Result.Elong >= 180 {
		Result.Phase = -Result.Phase // {waning moon}
		Result.Elong = -I
	} else {
		Result.Elong = I
	}
	return Result
} // MoonPosPrim

// MoonPos {-compute the J2000 RA/Declination of the moon}
func MoonPos(UT stdate.TStDateTimeRec) TStMoonPosRec {
	var Result TStMoonPosRec
	if !CheckDate(UT) {
		Result.RA = -1
		Result.DC = -1
		return Result
	}
	Result = MoonPosPrim(UT)
	return Result
}

// MoonRiseSet {compute the Moon rise and set time}
// {the value for H0 accounts for approximate refraction of 0.5667 deg., }
// {that rise or set is based on the upper limb instead of the center of the}
// {lunar disc, and the lunar parallax. In accordance with American Ephemeris }
// {practice, the phase of the moon is not taken into account, i.e., the time}
// {is based on the upper limb whether it is lit or not}
func MoonRiseSet(LD stdate.TStDate, Longitude, Latitude float64) TStRiseSetRec {
	var (
		H0     float64
		UT     stdate.TStDateTimeRec
		RP     TStPosRecArray
		MPR    TStMoonPosRec
		Result TStRiseSetRec
	)
	H0 = 0.125 // { default value }

	LD--
	UT.T = 0
	UT.D = LD

	if CheckDate(UT) {
		UT.D = UT.D + 2
		if !CheckDate(UT) {
			Result.ORise = -4
			Result.OSet = -4
			return Result
		}
		UT.D = UT.D - 2
	} else {
		Result.ORise = -4
		Result.OSet = -4
		return Result
	}

	for i := 0; i < 3; i++ {
		MPR = MoonPos(UT)
		RP[i].RA = MPR.RA
		RP[i].DC = MPR.DC
		if i >= 1 {
			if i == 1 {
				H0 = 0.7275*MPR.Plx - 0.5667
			}
			if RP[i].RA < RP[i-1].RA {
				RP[i].RA = RP[i].RA + 360
			}
		}
		UT.D++
	}
	Result = _RiseSetPrim(LD, Longitude, Latitude, H0, RP, false)
	return Result
} // MoonRiseSet

// LunarPhase {-compute the phase of the moon}
// {The value is positive if between New and Full Moon}
// {             negative if between Full and New Moon}
func LunarPhase(UT stdate.TStDateTimeRec) float64 {
	MPR := MoonPosPrim(UT)
	return MPR.Phase
} // LunarPhase

func _AddCor(K, J2 float64) float64 {
	Result := 0.000325 * math.Sin((299.77+0.107408*K-0.009173*J2)/radcor)
	Result += 0.000165 * math.Sin((251.88+0.016321*K)/radcor)
	Result += 0.000164 * math.Sin((251.83+26.651886*K)/radcor)
	Result += 0.000126 * math.Sin((349.42+36.412478*K)/radcor)
	Result += 0.000110 * math.Sin((84.660+18.206239*K)/radcor)
	Result += 0.000062 * math.Sin((141.74+53.303771*K)/radcor)
	Result += 0.000060 * math.Sin((207.14+2.453732*K)/radcor)
	Result += 0.000056 * math.Sin((154.84+7.306860*K)/radcor)
	Result += 0.000047 * math.Sin((34.520+27.261239*K)/radcor)
	Result += 0.000042 * math.Sin((207.19+0.121824*K)/radcor)
	Result += 0.000040 * math.Sin((291.34+1.844379*K)/radcor)
	Result += 0.000037 * math.Sin((161.72+24.198154*K)/radcor)
	Result += 0.000035 * math.Sin((239.56+25.513099*K)/radcor)
	Result += 0.000023 * math.Sin((331.55+3.592518*K)/radcor)
	return Result
} // _AddCor

// GetPhases {primitive routine to find the date/time of phases in a lunar cycle}
func GetPhases(K float64, PR *TStPhaseRecord) {
	var (
		JD, NK, TD, J1, J2, J3 float64
		E, FP, S1, M1, M2, M3  float64
	)
	NK = K
	pr := TStPhaseRecord{}
	// FillChar(PR, SizeOf(TStPhaseRecord), #0);
	for step := 0; step < 4; step++ {
		K = NK + (float64(step) * 0.25)
		FP = Frac(K)
		if FP < 0 {
			FP = FP + 1
		}

		// {compute Julian Centuries}
		J1 = K / 1236.85
		J2 = common.Sqr(J1)
		J3 = J2 * J1

		// {solar mean anomaly}
		S1 = 2.5534 + 29.1053569*K - 0.0000218*J2 - 0.00000011*J3
		S1 = Frac(S1/360.0) * 360
		if S1 < 0 {
			S1 = S1 + 360.0
		}

		// {lunar mean anomaly}
		M1 = 201.5643 + 385.81693528*K + 0.0107438*J2 + 0.00001239*J3 - 0.000000058*J2*J2
		M1 = Frac(M1/360.0) * 360
		if M1 < 0 {
			M1 = M1 + 360.0
		}

		// {lunar argument of latitude}
		M2 = 160.7108 + 390.67050274*K - 0.0016341*J2 - 0.00000227*J3 + 0.000000011*J2*J2
		M2 = Frac(M2/360.0) * 360
		if M2 < 0 {
			M2 = M2 + 360.0
		}

		// {lunar ascending node}
		M3 = 124.7746 - 1.56375580*K + 0.0020691*J2 + 0.00000215*J3
		M3 = Frac(M3/360.0) * 360
		if M3 < 0 {
			M3 = M3 + 360.0
		}

		// {convert to radians}
		S1 = S1 / radcor
		M1 = M1 / radcor
		M2 = M2 / radcor
		M3 = M3 / radcor

		// {mean Julian Date for phase}
		JD = 2451550.09765 + 29.530588853*K + 0.0001337*J2 - 0.000000150*J3 + 0.00000000073*J2*J2

		// {earth's orbital eccentricity}
		E = 1.0 - 0.002516*J1 - 0.0000074*J2

		// {New Moon date time}
		if FP < 0.01 {
			TD = -0.40720 * math.Sin(M1)
			TD += 0.17241 * E * math.Sin(S1)
			TD += 0.01608 * math.Sin(2*M1)
			TD += 0.01039 * math.Sin(2*M2)
			TD += 0.00739 * E * math.Sin(M1-S1)
			TD -= 0.00514 * E * math.Sin(M1+S1)
			TD += 0.00208 * E * E * math.Sin(2*S1)
			TD -= 0.00111 * math.Sin(M1-2*M2)
			TD -= 0.00057 * math.Sin(M1+2*M2)
			TD += 0.00056 * E * math.Sin(2*M1+S1)
			TD -= 0.00042 * math.Sin(3*M1)
			TD += 0.00042 * E * math.Sin(S1+2*M2)
			TD += 0.00038 * E * math.Sin(S1-2*M2)
			TD -= 0.00024 * E * math.Sin(2*(M1-S1))
			TD -= 0.00017 * math.Sin(M3)
			TD -= 0.00007 * math.Sin(M1+2*S1)
			JD = JD + TD + _AddCor(K, J2)
			pr.NMDate = JD
		}

		// {Full Moon date/time}
		if math.Abs(FP-0.5) < 0.01 {
			TD = -0.40614 * math.Sin(M1)
			TD += 0.17302 * E * math.Sin(S1)
			TD += 0.01614 * math.Sin(2*M1)
			TD += 0.01043 * math.Sin(2*M2)
			TD += 0.00734 * E * math.Sin(M1-S1)
			TD -= 0.00515 * E * math.Sin(M1+S1)
			TD += 0.00209 * E * E * math.Sin(2*S1)
			TD -= 0.00111 * math.Sin(M1-2*M2)
			TD -= 0.00057 * math.Sin(M1+2*M2)
			TD += 0.00056 * E * math.Sin(2*M1+S1)
			TD -= 0.00042 * math.Sin(3*M1)
			TD += 0.00042 * E * math.Sin(S1+2*M2)
			TD += 0.00038 * E * math.Sin(S1-2*M2)
			TD -= 0.00024 * E * math.Sin(2*(M1-S1))
			TD -= 0.00017 * math.Sin(M3)
			TD -= 0.00007 * math.Sin(M1+2*S1)
			JD = JD + TD + _AddCor(K, J2)
			pr.FMDate = JD
		}

		// {Quarters date/time}
		if (math.Abs(FP-0.25) < 0.01) || (math.Abs(FP-0.75) < 0.01) {
			TD = -0.62801 * math.Sin(M1)
			TD += 0.17172 * math.Sin(S1) * E
			TD -= 0.01183 * math.Sin(M1+S1) * E
			TD += 0.00862 * math.Sin(2*M1)
			TD += 0.00804 * math.Sin(2*M2)
			TD += 0.00454 * math.Sin(M1-S1) * E
			TD += 0.00204 * math.Sin(2*S1) * E * E
			TD -= 0.00180 * math.Sin(M1-2*M2)
			TD -= 0.00070 * math.Sin(M1+2*M2)
			TD -= 0.00040 * math.Sin(3*M1)
			TD -= 0.00034 * math.Sin(2*M1-S1) * E
			TD += 0.00032 * math.Sin(S1+2*M2) * E
			TD += 0.00032 * math.Sin(S1-2*M2) * E
			TD -= 0.00028 * math.Sin(M1+2*S1) * E * E
			TD += 0.00027 * math.Sin(2*M1+S1) * E
			TD -= 0.00017 * math.Sin(M3)
			TD -= 0.00005 * math.Sin(M1-S1-2*M2)
			JD = JD + TD + _AddCor(K, J2)

			// {adjustment to computed Julian Date}
			TD = 0.00306
			TD -= 0.00038 * E * math.Cos(S1)
			TD += 0.00026 * math.Cos(M1)
			TD -= 0.00002 * math.Cos(M1-S1)
			TD += 0.00002 * math.Cos(M1+S1)
			TD += 0.00002 * math.Cos(2*M2)

			if math.Abs(FP-0.25) < 0.01 {
				pr.FQDate = JD + TD
			} else {
				pr.LQDate = JD - TD
			}
		}
	}
	*PR = pr
} // GetPhases

// PhasePrim {-primitive phase calculation}
func PhasePrim(LD stdate.TStDate, PhaseArray TStPhaseArray) {
	_, _, Y := stdate.StDateToDMY(LD)
	LYear := float64(Y) - 0.05
	K := (LYear - 2000) * 12.3685
	K = math.Trunc(K)
	TD := K/12.3685 + 2000
	if TD > float64(Y) {
		K = K - 1
	}
	// {compute phases for each lunar cycle throughout the year}
	for i := 0; i < 14; i++ {
		GetPhases(K, &PhaseArray[i])
		K = K + 1
	}
}

// GenSearchPhase {searches for the specified phase in the given month/year expressed by SD}
func GenSearchPhase(SD stdate.TStDate, PV byte) TStLunarRecord {
	var (
		JD         float64
		PhaseArray TStPhaseArray
		Result     TStLunarRecord
	)
	C := 0
	Result = TStLunarRecord{T: [2]stdate.TStDateTimeRec{{D: stdate.BadDate, T: stdate.BadTime}, {D: stdate.BadDate, T: stdate.BadTime}}}
	// FillChar(Result, SizeOf(Result), $FF);

	_, LM, _ := stdate.StDateToDMY(SD)
	PhasePrim(SD, PhaseArray)
	for i := LM - 1; i <= LM+1; i++ {
		if PV == 0 {
			JD = PhaseArray[i].NMDate
		} else if PV == 1 {
			JD = PhaseArray[i].FQDate
		} else if PV == 2 {
			JD = PhaseArray[i].FMDate
		} else {
			JD = PhaseArray[i].LQDate
		}
		ADate := stdate.AstJulianDateToStDate(JD, true)

		_, TM, _ := stdate.StDateToDMY(ADate)
		if TM < LM {
			continue
		} else if TM == LM {
			Result.T[C].D = ADate
			Result.T[C].T = common.Trunc((Frac(JD) + 0.5) * 86400)
			if Result.T[C].T >= stdate.SecondsInDay {
				Result.T[C].T -= stdate.SecondsInDay
			}
			C++
		}
	}
	return Result
} // GenSearchPhase

// FirstQuarter {-compute date/time of FirstQuarter(s)}
func FirstQuarter(D stdate.TStDate) TStLunarRecord {
	return GenSearchPhase(D, 1)
}

// FullMoon {-compute the date/time of FullMoon(s)}
func FullMoon(D stdate.TStDate) TStLunarRecord {
	return GenSearchPhase(D, 2)
}

// LastQuarter {-compute the date/time of LastQuarter(s)}
func LastQuarter(D stdate.TStDate) TStLunarRecord {
	return GenSearchPhase(D, 3)
}

// NewMoon {-compute the date/time of NewMoon(s)}
func NewMoon(D stdate.TStDate) TStLunarRecord {
	return GenSearchPhase(D, 0)
}

func _NextPrevPhase(D stdate.TStDate, Ph byte, FindNext bool) stdate.TStDateTimeRec {
	var (
		K, JD, TJD float64
		PR         TStPhaseRecord
		OK         bool
		Result     stdate.TStDateTimeRec
	)
	if (D < stdate.MinDate) || (D > stdate.MaxDate) {
		Result.D = stdate.BadDate
		Result.T = stdate.BadTime
		return Result
	}

	LD, LM, LY := stdate.StDateToDMY(D)
	K = ((float64(LY)+float64(LM)/12+float64(LD)/365.25)-2000)*12.3685 - 0.5
	if FindNext {
		K = math.Round(K) - 1
	} else {
		K = math.Round(K) - 2
	}

	OK = false
	TJD = stdate.AstJulianDate(D)
	for {
		GetPhases(K, &PR)

		if Ph == 0 {
			JD = PR.NMDate
		} else if Ph == 1 {
			JD = PR.FQDate
		} else if Ph == 2 {
			JD = PR.FMDate
		} else {
			JD = PR.LQDate
		}

		if FindNext {
			if JD > TJD {
				OK = true
			} else {
				K = K + 1
			}
		} else {
			if JD < TJD {
				OK = true
			} else {
				K = K - 1
			}
		}
		if OK {
			break
		}
	}

	Result.D = stdate.AstJulianDateToStDate(JD, true)
	if Result.D != stdate.BadDate {
		Result.T = common.Trunc((Frac(JD) + 0.5) * 86400)
		if Result.T >= stdate.SecondsInDay {
			Result.T -= stdate.SecondsInDay
		}
	} else {
		Result.T = stdate.BadTime
	}
	return Result
} // _NextPrevPhase

// NextFirstQuarter {-compute the date/time of the next closest FirstQuarter}
func NextFirstQuarter(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 1, true)
}

// NextFullMoon {-compute the date/time of the next closest FullMoon}
func NextFullMoon(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 2, true)
}

// NextLastQuarter {-compute the date/time of the next closest LastQuarter}
func NextLastQuarter(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 3, true)
}

// NextNewMoon {-compute the date/time of the next closest NewMoon}
func NextNewMoon(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 0, true)
}

// PrevFirstQuarter {-compute the date/time of the prev closest FirstQuarter}
func PrevFirstQuarter(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 1, false)
}

// PrevFullMoon {-compute the date/time of the prev closest FullMoon}
func PrevFullMoon(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 2, false)
}

// PrevLastQuarter {-compute the date/time of the prev closest LastQuarter}
func PrevLastQuarter(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 3, false)
}

// PrevNewMoon {-compute the date/time of the prev closest NewMoon}
func PrevNewMoon(D stdate.TStDate) stdate.TStDateTimeRec {
	return _NextPrevPhase(D, 0, false)
}
