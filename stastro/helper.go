package stastro

import (
	"math"

	"gitlab.com/solsw/delphi"
	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

var _AJDOffSet float64

func init() {
	dt, _ := delphi.EncodeDate(1600, 1, 1)
	_AJDOffSet = stdate.AstJulianDatePrim(1600, 1, 1, 0) - dt
}

// Frac returns the fractional part of 'x'.
func Frac(x float64) float64 {
	_, f := math.Modf(x)
	return f
}

// CheckDate reports whether 'UT' contains valid date and time.
func CheckDate(UT stdate.TStDateTimeRec) bool {
	if UT.D < stdate.MinDate || UT.D > stdate.MaxDate || UT.T < 0 || UT.T > stdate.MaxTime {
		return false
	}
	return true
}

// CheckYear computes year from 'Y' and 'Epoch'.
func CheckYear(Y, Epoch int) int {
	if Y < 100 {
		if Y >= (Epoch % 100) {
			return ((Epoch / 100) * 100) + Y
		}
		return ((Epoch / 100) * 100) + 100 + Y
	}
	return Y
}

// SiderealTime {-compute Sidereal Time at Greenwich in degrees}
func SiderealTime(UT stdate.TStDateTimeRec) float64 {
	var T, JD float64
	if !CheckDate(UT) {
		return -1
	}
	JD = stdate.AstJulianDate(UT.D) + float64(UT.T)/86400
	T = (JD - 2451545.0) / 36525.0
	Result := 280.46061837 + 360.98564736629*(JD-2451545.0) + 0.000387933*common.Sqr(T) - (common.Sqr(T) * T / 38710000)
	Result = Frac(Result/360.0) * 360.0
	if Result < 0 {
		Result = 360 + Result
	}
	return Result
}

func DateTimeToAJD(D delphi.TDateTime) float64 {
	return D + _AJDOffSet
}
