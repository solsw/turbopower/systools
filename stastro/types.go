package stastro

import (
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

// StAstro types
type (
	TStRiseSetRec struct {
		ORise, OSet stdate.TStTime
	}

	TStPosRec struct {
		RA, DC float64
	}
	TStPosRecArray = [3]TStPosRec

	TStSunXYZRec struct {
		SunX, SunY, SunZ, RV, SLong, SLat float64
	}

	TStLunarRecord struct {
		T [2]stdate.TStDateTimeRec
	}

	TStPhaseRecord struct {
		NMDate, FQDate, FMDate, LQDate float64
	}
	TStPhaseArray = [14]TStPhaseRecord

	TStMoonPosRec struct {
		RA, DC, Phase, Dia, Plx, Elong float64
	}

	TStTwilight = int

	TStEclipticalCord struct {
		L0, B0, R0 float64
	}

	TStRectangularCord struct {
		X, Y, Z float64
	}

	TStPlanetsRec struct {
		RA, DC, Elong float64
	}
	TStPlanetsArray = [8]TStPlanetsRec

	TStJupSatPos struct {
		X, Y float64
	}

	TStJupSats struct {
		Io, Europa, Ganymede, Callisto TStJupSatPos
	}
)
