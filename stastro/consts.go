package stastro

const (
	// {number of degrees in a radian}
	radcor float64 = 57.29577951308232

	// StdDate {Ast. Julian Date for J2000 Epoch}
	StdDate float64 = 2451545.0

	// OB2000 {J2000 obliquity of the ecliptic (radians)}
	OB2000 = 0.409092804
)

const (
	ttCivil TStTwilight = iota
	ttNautical
	ttAstronomical
)
