package stastro

import (
	"math"

	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
	"gitlab.com/solsw/delphi"
)

type SunCoordsRec struct {
	X, Y, Z, L, B, R float64
}

type TranformRec struct {
	A, B, C [6]float64
}

func _SunCoords(JD float64) SunCoordsRec {
	var (
		L, B, R, T0, TM, RS, OB, A float64
		Result                     SunCoordsRec
	)
	T0 = (JD - StdDate) / 365250
	TM = T0 / 100
	RS = radcor * 3600
	OB = 0.4090928042223 - 4680.93/RS*TM
	OB = OB - 1.55/RS*common.Sqr(TM)
	OB = OB + 1999.25/RS*common.Sqr(TM)*TM
	OB = OB - 51.38/RS*common.Sqr(common.Sqr(TM))
	OB = OB - 249.67/RS*common.Sqr(common.Sqr(TM))*TM
	OB = OB - 39.05/RS*common.Sqr(common.Sqr(TM))*common.Sqr(TM)
	OB = OB + 7.12/RS*common.Sqr(common.Sqr(TM))*common.Sqr(TM)*TM
	OB = OB + 27.87/RS*common.Sqr(common.Sqr(common.Sqr(TM)))

	L = 175347046 + 3341656*math.Cos(4.6692568+6283.07585*T0)
	L = L + 34894*math.Cos(4.6261000+12566.1517*T0)
	L = L + 3497*math.Cos(2.7441000+5753.3849*T0)
	L = L + 3418*math.Cos(2.8289000+3.5231*T0)
	L = L + 3136*math.Cos(3.6277000+77713.7715*T0)
	L = L + 2676*math.Cos(4.4181000+7860.4194*T0)
	L = L + 2343*math.Cos(6.1352000+3930.2097*T0)
	L = L + 1324*math.Cos(0.7425000+11506.7698*T0)
	L = L + 1273*math.Cos(2.0371000+529.6910*T0)
	L = L + 1199*math.Cos(1.1096000+1577.3435*T0)
	L = L + 990*math.Cos(5.2330000+5884.9270*T0)
	L = L + 902*math.Cos(2.0450000+26.1490*T0)
	L = L + 857*math.Cos(3.5080000+398.149*T0)
	L = L + 780*math.Cos(1.1790000+5223.694*T0)
	L = L + 753*math.Cos(2.5330000+5507.553*T0)
	L = L + 505*math.Cos(4.5830000+18849.228*T0)
	L = L + 492*math.Cos(4.2050000+775.523*T0)
	L = L + 357*math.Cos(2.9200000+0.067*T0)
	L = L + 317*math.Cos(5.8490000+11790.626*T0)
	L = L + 284*math.Cos(1.8990000+796.298*T0)
	L = L + 271*math.Cos(0.3150000+10977.079*T0)
	L = L + 243*math.Cos(0.3450000+5486.778*T0)
	L = L + 206*math.Cos(4.8060000+2544.314*T0)
	L = L + 205*math.Cos(1.8690000+5573.143*T0)
	L = L + 202*math.Cos(2.4580000+6069.777*T0)
	L = L + 156*math.Cos(0.8330000+213.299*T0)
	L = L + 132*math.Cos(3.4110000+2942.463*T0)
	L = L + 126*math.Cos(1.0830000+20.775*T0)
	L = L + 115*math.Cos(0.6450000+0.980*T0)
	L = L + 103*math.Cos(0.6360000+4694.003*T0)
	L = L + 102*math.Cos(0.9760000+15720.839*T0)
	L = L + 102*math.Cos(4.2670000+7.114*T0)
	L = L + 99*math.Cos(6.2100000+2146.170*T0)
	L = L + 98*math.Cos(0.6800000+155.420*T0)
	L = L + 86*math.Cos(5.9800000+161000.690*T0)
	L = L + 85*math.Cos(1.3000000+6275.960*T0)
	L = L + 85*math.Cos(3.6700000+71430.700*T0)
	L = L + 80*math.Cos(1.8100000+17260.150*T0)

	A = 628331966747.0 + 206059*math.Cos(2.678235+6283.07585*T0)
	A = A + 4303*math.Cos(2.635100+12566.1517*T0)
	A = A + 425*math.Cos(1.590000+3.523*T0)
	A = A + 119*math.Cos(5.796000+26.298*T0)
	A = A + 109*math.Cos(2.966000+1577.344*T0)
	A = A + 93*math.Cos(2.590000+18849.23*T0)
	A = A + 72*math.Cos(1.140000+529.69*T0)
	A = A + 68*math.Cos(1.870000+398.15*T0)
	A = A + 67*math.Cos(4.410000+5507.55*T0)
	A = A + 59*math.Cos(2.890000+5223.69*T0)
	A = A + 56*math.Cos(2.170000+155.42*T0)
	A = A + 45*math.Cos(0.400000+796.30*T0)
	A = A + 36*math.Cos(0.470000+775.52*T0)
	A = A + 29*math.Cos(2.650000+7.11*T0)
	A = A + 21*math.Cos(5.340000+0.98*T0)
	A = A + 19*math.Cos(1.850000+5486.78*T0)
	A = A + 19*math.Cos(4.970000+213.30*T0)
	A = A + 17*math.Cos(2.990000+6275.96*T0)
	A = A + 16*math.Cos(0.030000+2544.31*T0)
	L = L + (A * T0)

	A = 52919 + 8720*math.Cos(1.0721+6283.0758*T0)
	A = A + 309*math.Cos(0.867+12566.152*T0)
	A = A + 27*math.Cos(0.050+3.52*T0)
	A = A + 16*math.Cos(5.190+26.30*T0)
	A = A + 16*math.Cos(3.68+155.42*T0)
	A = A + 10*math.Cos(0.76+18849.23*T0)
	A = A + 9*math.Cos(2.06+77713.77*T0)
	A = A + 7*math.Cos(0.83+775.52*T0)
	A = A + 5*math.Cos(4.66+1577.34*T0)
	L = L + (A * common.Sqr(T0))

	A = 289*math.Cos(5.844+6283.076*T0) + 35
	A = A + 17*math.Cos(5.49+12566.15*T0)
	A = A + 3*math.Cos(5.20+155.42*T0)
	A = A + 1*math.Cos(4.72+3.52*T0)
	L = L + (A * common.Sqr(T0) * T0)

	A = 114 * math.Cos(3.142)
	L = L + (A * common.Sqr(common.Sqr(T0)))
	L = L / 1.0e+8

	// {solar latitude}
	B = 280 * math.Cos(3.199+84334.662*T0)
	B = B + 102*math.Cos(5.422+5507.553*T0)
	B = B + 80*math.Cos(3.88+5223.69*T0)
	B = B + 44*math.Cos(3.70+2352.87*T0)
	B = B + 32*math.Cos(4.00+1577.34*T0)

	A = 9*math.Cos(3.90+5507.550*T0) + 6*math.Cos(1.73+5223.690*T0)
	B = B + (A * T0)
	B = B / 1.0e+8

	// {solar radius vector (astronomical units)}
	R = 100013989 + 1670700*math.Cos(3.0984635+6283.07585*T0)
	R = R + 13956*math.Cos(3.05525+12566.15170*T0)
	R = R + 3084*math.Cos(5.1985+77713.7715*T0)
	R = R + 1628*math.Cos(1.1739+5753.3849*T0)
	R = R + 1576*math.Cos(2.8649+7860.4194*T0)
	R = R + 925*math.Cos(5.453+11506.770*T0)
	R = R + 542*math.Cos(4.564+3930.210*T0)
	R = R + 472*math.Cos(3.661+5884.927*T0)
	R = R + 346*math.Cos(0.964+5507.553*T0)
	R = R + 329*math.Cos(5.900+5223.694*T0)
	R = R + 307*math.Cos(0.299+5573.143*T0)
	R = R + 243*math.Cos(4.273+11790.629*T0)
	R = R + 212*math.Cos(5.847+1577.344*T0)
	R = R + 186*math.Cos(5.022+10977.079*T0)
	R = R + 175*math.Cos(3.012+18849.228*T0)
	R = R + 110*math.Cos(5.055+5486.778*T0)
	R = R + 98*math.Cos(0.89+6069.78*T0)
	R = R + 86*math.Cos(5.69+15720.84*T0)
	R = R + 86*math.Cos(1.27+161000.69*T0)
	R = R + 65*math.Cos(0.27+17260.15*T0)
	R = R + 63*math.Cos(0.92+529.69*T0)
	R = R + 57*math.Cos(2.01+83996.85*T0)
	R = R + 56*math.Cos(5.24+71430.70*T0)
	R = R + 49*math.Cos(3.25+2544.31*T0)
	R = R + 47*math.Cos(2.58+775.52*T0)
	R = R + 45*math.Cos(5.54+9437.76*T0)
	R = R + 43*math.Cos(6.01+6275.96*T0)
	R = R + 39*math.Cos(5.36+4694.00*T0)
	R = R + 38*math.Cos(2.39+8827.39*T0)
	R = R + 37*math.Cos(0.83+19651.05*T0)
	R = R + 37*math.Cos(4.90+12139.55*T0)
	R = R + 36*math.Cos(1.67+12036.46*T0)
	R = R + 35*math.Cos(1.84+2942.46*T0)
	R = R + 33*math.Cos(0.24+7084.90*T0)
	R = R + 32*math.Cos(0.18+5088.63*T0)
	R = R + 32*math.Cos(1.78+398.15*T0)
	R = R + 28*math.Cos(1.21+6286.60*T0)
	R = R + 28*math.Cos(1.90+6279.55*T0)
	R = R + 26*math.Cos(4.59+10447.39*T0)
	R = R / 1.0e+8

	A = 103019 * math.Cos(1.107490+6283.075850*T0)
	A = A + 1721*math.Cos(1.0644+12566.1517*T0)
	A = A + 702*math.Cos(3.142)
	A = A + 32*math.Cos(1.02+18849.23*T0)
	A = A + 31*math.Cos(2.84+5507.55*T0)
	A = A + 25*math.Cos(1.32+5223.69*T0)
	A = A + 18*math.Cos(1.42+1577.34*T0)
	A = A + 10*math.Cos(5.91+10977.08*T0)
	A = A + 9*math.Cos(1.42+6275.96*T0)
	A = A + 9*math.Cos(0.27+5486.78*T0)
	R = R + (A * T0 / 1.0e+8)

	A = 4359 * math.Cos(5.7846+6283.0758*T0)
	A = A + 124*math.Cos(5.579+12566.152*T0)
	A = A + 12*math.Cos(3.14)
	A = A + 9*math.Cos(3.63+77713.77*T0)
	A = A + 6*math.Cos(1.87+5573.14*T0)
	A = A + 3*math.Cos(5.47+18849.23*T0)
	R = R + (A * common.Sqr(T0) / 1.0e+8)

	L = (L + math.Pi)
	L = Frac(L/2.0/math.Pi) * 2.0 * math.Pi
	if L < 0 {
		L = L + (2.0 * math.Pi)
	}
	B = -B

	Result.L = L
	Result.B = B
	Result.R = R
	Result.X = R * math.Cos(B) * math.Cos(L)
	Result.Y = R * (math.Cos(B)*math.Sin(L)*math.Cos(OB) - math.Sin(B)*math.Sin(OB))
	Result.Z = R * (math.Cos(B)*math.Sin(L)*math.Sin(OB) + math.Sin(B)*math.Cos(OB))
	return Result
}

func _JupSatsLo(AJD float64) TStJupSats {
	var Result TStJupSats
	AJD = DateTimeToAJD(AJD)
	DateDif := AJD - 2451545.0
	ArgJup := 172.74 + (0.00111588 * DateDif)
	ArgJup = Frac(ArgJup/360.0) * 360.0
	if ArgJup < 0 {
		ArgJup = 360.0 + ArgJup
	}
	ArgJup = ArgJup / radcor

	AnomE := 357.529 + (0.9856003 * DateDif)
	AnomE = Frac(AnomE/360.0) * 360.0
	if AnomE < 0 {
		AnomE = 360.0 + AnomE
	}
	AnomE = AnomE / radcor

	AnomJ := 20.020 + (0.0830853*DateDif + (0.329 * math.Sin(ArgJup)))
	AnomJ = Frac(AnomJ/360.0) * 360.0
	if AnomJ < 0 {
		AnomJ = 360.0 + AnomJ
	}
	AnomJ = AnomJ / radcor

	DeltaLong := 66.115 + (0.9025179*DateDif - (0.329 * math.Sin(ArgJup)))
	DeltaLong = Frac(DeltaLong/360.0) * 360.0
	if DeltaLong < 0 {
		DeltaLong = 360.0 + DeltaLong
	}
	DeltaLong = DeltaLong / radcor

	ECenterE := 1.915*math.Sin(AnomE) + 0.020*math.Sin(2*AnomE)
	ECenterE = ECenterE / radcor

	ECenterJ := 5.555*math.Sin(AnomJ) + 0.168*math.Sin(2*AnomJ)
	ECenterJ = ECenterJ / radcor

	K := (DeltaLong + ECenterE - ECenterJ)

	RVE := 1.00014 - (0.01671 * math.Cos(AnomE)) - (0.00014 * math.Cos(2*AnomE))
	RVJ := 5.20872 - (0.25208 * math.Cos(AnomJ)) - (0.00611 * math.Cos(2*AnomJ))

	EJDist := math.Sqrt(common.Sqr(RVJ) + common.Sqr(RVE) - (2 * RVJ * RVE * math.Cos(K)))

	Phase := RVE / EJDist * math.Sin(K)
	Phase = math.Asin(Phase)

	if ((math.Sin(K) < 0) && (Phase > 0)) ||
		((math.Sin(K) > 0) && (Phase < 0)) {
		Phase = -Phase
	}

	Lambda := 34.35 + (0.083091 * DateDif) + (0.329 * math.Sin(ArgJup))
	Lambda = Lambda/radcor + ECenterJ

	DS := 3.12 * math.Sin(Lambda+42.8/radcor)
	DE := DS - 2.22*math.Sin(Phase)*math.Cos(Lambda+22/radcor) - 1.30*((RVJ-EJDist)/EJDist)*math.Sin(Lambda-100.5/radcor)
	DE = DE / radcor

	Mu1 := 163.8067 + 203.4058643*(DateDif-(EJDist/173))
	Mu1 = Frac(Mu1/360.0) * 360.0
	if Mu1 < 0 {
		Mu1 = 360.0 + Mu1
	}
	Mu1 = Mu1/radcor + Phase - ECenterJ

	Mu2 := 358.4108 + 101.2916334*(DateDif-(EJDist/173))
	Mu2 = Frac(Mu2/360.0) * 360.0
	if Mu2 < 0 {
		Mu2 = 360.0 + Mu2
	}
	Mu2 = Mu2/radcor + Phase - ECenterJ

	Mu3 := 5.7129 + 50.2345179*(DateDif-(EJDist/173))
	Mu3 = Frac(Mu3/360.0) * 360.0
	if Mu3 < 0 {
		Mu3 = 360.0 + Mu3
	}
	Mu3 = Mu3/radcor + Phase - ECenterJ

	Mu4 := 224.8151 + 21.4879801*(DateDif-(EJDist/173))
	Mu4 = Frac(Mu4/360.0) * 360.0
	if Mu4 < 0 {
		Mu4 = 360.0 + Mu4
	}
	Mu4 = Mu4/radcor + Phase - ECenterJ

	G := 331.18 + 50.310482*(DateDif-(EJDist/173))
	G = Frac(G/360.0) * 360.0
	if G < 0 {
		G = 360.0 + G
	}
	G = G / radcor
	H := 87.40 + 21.569231*(DateDif-(EJDist/173))
	H = Frac(H/360.0) * 360.0
	if H < 0 {
		H = 360.0 + H
	}
	H = H / radcor

	TmpDbl1 := 0.473 * math.Sin(2*(Mu1-Mu2)) / radcor
	TmpDbl2 := 1.065 * math.Sin(2*(Mu2-Mu3)) / radcor

	R1 := 5.9073 - 0.0244*math.Cos(2*(Mu1-Mu2))
	R2 := 9.3991 - 0.0882*math.Cos(2*(Mu2-Mu3))
	R3 := 14.9924 - 0.0216*math.Cos(G)
	R4 := 26.3699 - 0.1935*math.Cos(H)

	Mu1 = Mu1 + TmpDbl1
	Mu2 = Mu2 + TmpDbl2
	Mu3 = Mu3 + (0.165*math.Sin(G))/radcor
	Mu4 = Mu4 + (0.841*math.Sin(H))/radcor

	Result.Io.X = R1 * math.Sin(Mu1)
	Result.Io.Y = -R1 * math.Cos(Mu1) * math.Sin(DE)

	Result.Europa.X = R2 * math.Sin(Mu2)
	Result.Europa.Y = -R2 * math.Cos(Mu2) * math.Sin(DE)

	Result.Ganymede.X = R3 * math.Sin(Mu3)
	Result.Ganymede.Y = -R3 * math.Cos(Mu3) * math.Sin(DE)

	Result.Callisto.X = R4 * math.Sin(Mu4)
	Result.Callisto.Y = -R4 * math.Cos(Mu4) * math.Sin(DE)
	return Result
}

func _JupSatsHi(AJD float64, Shadows bool) TStJupSats {
	var (
		SunPos SunCoordsRec
		JupPos TStEclipticalCord

		SatX, SatY, SatZ [5]float64

		TD1, TD2, Angle, AJDT, JupX, JupY, JupZ, EJDist, Jup1, Jup2, DateDif,
		L1, L2, L3, L4, Pi1, Pi2, Pi3, Pi4, W1, W2, W3, W4,
		Inequality, PhiLambda, NodeJup, AnomJup, AnomSat, LongPerJ,
		S1, S2, S3, S4, TL1, TL2, TL3, TL4, B1, B2, B3, B4, R1, R2, R3, R4, T0,
		Precession, Inclination float64
		Transforms [5]TranformRec
		Result     TStJupSats
	)
	AJD = DateTimeToAJD(AJD)
	SunPos = _SunCoords(AJD)

	if !Shadows {
		TD1 = 5.0
		AJDT = AJD - 0.0057755183*TD1 // {first guess}
		for {
			JupPos = ComputeJupiter(AJDT)

			JupX = JupPos.R0*math.Cos(JupPos.B0)*math.Cos(JupPos.L0) + SunPos.R*math.Cos(SunPos.L)
			JupY = JupPos.R0*math.Cos(JupPos.B0)*math.Sin(JupPos.L0) + SunPos.R*math.Sin(SunPos.L)
			JupZ = JupPos.R0 * math.Sin(JupPos.B0)

			EJDist = math.Sqrt(common.Sqr(JupX) + common.Sqr(JupY) + common.Sqr(JupZ))
			TD2 = math.Abs(EJDist - TD1)
			if math.Abs(TD2) > 0.0005 {
				AJDT = AJD - 0.0057755183*((EJDist+TD1)/2)
				TD1 = EJDist
			}
			if TD2 <= 0.0005 {
				break
			}
		}
	} else {
		JupPos = ComputeJupiter(AJD)

		JupX = JupPos.R0 * math.Cos(JupPos.B0) * math.Cos(JupPos.L0)
		JupY = JupPos.R0 * math.Cos(JupPos.B0) * math.Sin(JupPos.L0)
		JupZ = JupPos.R0 * math.Sin(JupPos.B0)
		EJDist = math.Sqrt(common.Sqr(JupX+SunPos.X) + common.Sqr(JupY+SunPos.Y) + common.Sqr(JupZ+SunPos.Z))
	}

	Jup1 = math.Atan2(JupX, JupY)
	Jup2 = math.Atan(JupZ / math.Sqrt(common.Sqr(JupX)+common.Sqr(JupY)))

	DateDif = AJD - 2443000.5 - (0.0057755183 * EJDist)

	L1 = 106.07947 + 203.488955432*DateDif
	L1 = Frac(L1/360.0) * 360.0
	if L1 < 0 {
		L1 = 360.0 + L1
	}
	L1 = L1 / radcor

	L2 = 175.72938 + 101.374724550*DateDif
	L2 = Frac(L2/360.0) * 360.0
	if L2 < 0 {
		L2 = 360.0 + L2
	}
	L2 = L2 / radcor

	L3 = 120.55434 + 50.317609110*DateDif
	L3 = Frac(L3/360.0) * 360.0
	if L3 < 0 {
		L3 = 360.0 + L3
	}
	L3 = L3 / radcor

	L4 = 84.44868 + 21.571071314*DateDif
	L4 = Frac(L4/360.0) * 360.0
	if L4 < 0 {
		L4 = 360.0 + L4
	}
	L4 = L4 / radcor

	Pi1 = 58.3329 + 0.16103936*DateDif
	Pi1 = Frac(Pi1/360.0) * 360.0
	if Pi1 < 0 {
		Pi1 = 360.0 + Pi1
	}
	Pi1 = Pi1 / radcor

	Pi2 = 132.8959 + 0.04647985*DateDif
	Pi2 = Frac(Pi2/360.0) * 360.0
	if Pi2 < 0 {
		Pi2 = 360.0 + Pi2
	}
	Pi2 = Pi2 / radcor

	Pi3 = 187.2887 + 0.00712740*DateDif
	Pi3 = Frac(Pi3/360.0) * 360.0
	if Pi3 < 0 {
		Pi3 = 360.0 + Pi3
	}
	Pi3 = Pi3 / radcor

	Pi4 = 335.3418 + 0.00183998*DateDif
	Pi4 = Frac(Pi4/360.0) * 360.0
	if Pi4 < 0 {
		Pi4 = 360.0 + Pi4
	}
	Pi4 = Pi4 / radcor

	W1 = 311.0793 - 0.13279430*DateDif
	W1 = Frac(W1/360.0) * 360.0
	if W1 < 0 {
		W1 = 360.0 + W1
	}
	W1 = W1 / radcor

	W2 = 100.5099 - 0.03263047*DateDif
	W2 = Frac(W2/360.0) * 360.0
	if W2 < 0 {
		W2 = 360.0 + W2
	}
	W2 = W2 / radcor

	W3 = 119.1688 - 0.00717704*DateDif
	W3 = Frac(W3/360.0) * 360.0
	if W3 < 0 {
		W3 = 360.0 + W3
	}
	W3 = W3 / radcor

	W4 = 322.5729 - 0.00175934*DateDif
	W4 = Frac(W4/360.0) * 360.0
	if W4 < 0 {
		W4 = 360.0 + W4
	}
	W4 = W4 / radcor

	Inequality = 0.33033 * math.Sin((163.679+0.0010512*DateDif)/radcor)
	Inequality = Inequality + 0.03439*math.Sin((34.486-0.0161731*DateDif)/radcor)
	Inequality = Inequality / radcor

	PhiLambda = 191.8132 + 0.17390023*DateDif
	PhiLambda = Frac(PhiLambda/360.0) * 360.0
	if PhiLambda < 0 {
		PhiLambda = 360.0 + PhiLambda
	}
	PhiLambda = PhiLambda / radcor

	NodeJup = 316.5182 - 0.00000208*DateDif
	NodeJup = Frac(NodeJup/360.0) * 360.0
	if NodeJup < 0 {
		NodeJup = 360.0 + NodeJup
	}
	NodeJup = NodeJup / radcor

	AnomJup = 30.23756 + 0.0830925701*DateDif
	AnomJup = Frac(AnomJup/360.0) * 360.0
	if AnomJup < 0 {
		AnomJup = 360.0 + AnomJup
	}
	AnomJup = AnomJup/radcor + Inequality

	AnomSat = 31.97853 + 0.0334597339*DateDif
	AnomSat = Frac(AnomSat/360.0) * 360.0
	if AnomSat < 0 {
		AnomSat = 360.0 + AnomSat
	}
	AnomSat = AnomSat / radcor

	LongPerJ = 13.469942 / radcor

	S1 = 0.47259 * math.Sin(2*(L1-L2))
	S1 = S1 - 0.03480*math.Sin(Pi3-Pi4)
	S1 = S1 - 0.01756*math.Sin(Pi1+Pi3-2*LongPerJ-2*AnomJup)
	S1 = S1 + 0.01080*math.Sin(L2-2*L3+Pi3)
	S1 = S1 + 0.00757*math.Sin(PhiLambda)
	S1 = S1 + 0.00663*math.Sin(L2-2*L3+Pi4)
	S1 = S1 + 0.00453*math.Sin(L1-Pi3)
	S1 = S1 + 0.00453*math.Sin(L2-2*L3+Pi2)
	S1 = S1 - 0.00354*math.Sin(L1-L2)
	S1 = S1 - 0.00317*math.Sin(2*NodeJup-2*LongPerJ)
	S1 = S1 - 0.00269*math.Sin(L2-2*L3+Pi1)
	S1 = S1 + 0.00263*math.Sin(L1-Pi4)
	S1 = S1 + 0.00186*math.Sin(L1-Pi1)
	S1 = S1 - 0.00186*math.Sin(AnomJup)
	S1 = S1 + 0.00167*math.Sin(Pi2-Pi3)
	S1 = S1 + 0.00158*math.Sin(4*(L1-L2))
	S1 = S1 - 0.00155*math.Sin(L1-L3)
	S1 = S1 - 0.00142*math.Sin(NodeJup+W3-2*LongPerJ-2*AnomJup)
	S1 = S1 - 0.00115*math.Sin(2*(L1-2*L2+W2))
	S1 = S1 + 0.00089*math.Sin(Pi2-Pi4)
	S1 = S1 + 0.00084*math.Sin(W2-W3)
	S1 = S1 + 0.00084*math.Sin(L1+Pi3-2*LongPerJ-2*AnomJup)
	S1 = S1 + 0.00053*math.Sin(NodeJup-W2)

	S2 = 1.06476 * math.Sin(2*(L2-L3))
	S2 = S2 + 0.04253*math.Sin(L1-2*L2+Pi3)
	S2 = S2 + 0.03579*math.Sin(L2-Pi3)
	S2 = S2 + 0.02383*math.Sin(L1-2*L2+Pi4)
	S2 = S2 + 0.01977*math.Sin(L2-Pi4)
	S2 = S2 - 0.01843*math.Sin(PhiLambda)
	S2 = S2 + 0.01299*math.Sin(Pi3-Pi4)
	S2 = S2 - 0.01142*math.Sin(L2-L3)
	S2 = S2 + 0.01078*math.Sin(L2-Pi2)
	S2 = S2 - 0.01058*math.Sin(AnomJup)
	S2 = S2 + 0.00870*math.Sin(L2-2*L3+Pi2)
	S2 = S2 - 0.00775*math.Sin(2*(NodeJup-LongPerJ))
	S2 = S2 + 0.00524*math.Sin(2*(L1-L2))
	S2 = S2 - 0.00460*math.Sin(L1-L3)
	S2 = S2 + 0.00450*math.Sin(L2-2*L3+Pi1)
	S2 = S2 + 0.00327*math.Sin(NodeJup-2*AnomJup+W3-2*LongPerJ)
	S2 = S2 - 0.00296*math.Sin(Pi1+Pi3-2*LongPerJ-2*AnomJup)
	S2 = S2 - 0.00151*math.Sin(2*AnomJup)
	S2 = S2 + 0.00146*math.Sin(NodeJup-W3)
	S2 = S2 + 0.00125*math.Sin(NodeJup-W4)
	S2 = S2 - 0.00117*math.Sin(L1-2*L3+Pi3)
	S2 = S2 - 0.00095*math.Sin(2*(L2-W2))
	S2 = S2 + 0.00086*math.Sin(2*(L1-2*L2+W2))
	S2 = S2 - 0.00086*math.Sin(5*AnomSat-2*AnomJup+52.225/radcor)
	S2 = S2 - 0.00078*math.Sin(L2-L4)
	S2 = S2 - 0.00064*math.Sin(L1-2*L3+Pi4)
	S2 = S2 - 0.00063*math.Sin(3*L3-7*L4+4*Pi4)
	S2 = S2 + 0.00061*math.Sin(Pi1-Pi4)
	S2 = S2 + 0.00058*math.Sin(2*(NodeJup-LongPerJ-AnomJup))
	S2 = S2 + 0.00058*math.Sin(W3-W4)
	S2 = S2 + 0.00056*math.Sin(2*(L2-L4))
	S2 = S2 + 0.00055*math.Sin(2*(L1-L3))
	S2 = S2 + 0.00052*math.Sin(3*L3-7*L4+Pi3+3*Pi4)
	S2 = S2 - 0.00043*math.Sin(L1-Pi3)
	S2 = S2 + 0.00042*math.Sin(Pi3-Pi2)
	S2 = S2 + 0.00041*math.Sin(5*(L2-L3))
	S2 = S2 + 0.00041*math.Sin(Pi4-LongPerJ)
	S2 = S2 + 0.00038*math.Sin(L2-Pi1)
	S2 = S2 + 0.00032*math.Sin(W2-W3)
	S2 = S2 + 0.00032*math.Sin(2*(L3-AnomJup-LongPerJ))
	S2 = S2 + 0.00029*math.Sin(Pi1-Pi3)

	S3 = 0.16477 * math.Sin(L3-Pi3)
	S3 = S3 + 0.09062*math.Sin(L3-Pi4)
	S3 = S3 - 0.06907*math.Sin(L2-L3)
	S3 = S3 + 0.03786*math.Sin(Pi3-Pi4)
	S3 = S3 + 0.01844*math.Sin(2*(L3-L4))
	S3 = S3 - 0.01340*math.Sin(AnomJup)
	S3 = S3 + 0.00703*math.Sin(L2-2*L3+Pi3)
	S3 = S3 - 0.00670*math.Sin(2*(NodeJup-LongPerJ))
	S3 = S3 - 0.00540*math.Sin(L3-L4)
	S3 = S3 + 0.00481*math.Sin(Pi1+Pi3-2*LongPerJ-2*AnomJup)
	S3 = S3 - 0.00409*math.Sin(L2-2*L3+Pi2)
	S3 = S3 + 0.00379*math.Sin(L2-2*L3+Pi4)
	S3 = S3 + 0.00235*math.Sin(NodeJup-W3)
	S3 = S3 + 0.00198*math.Sin(NodeJup-W4)
	S3 = S3 + 0.00180*math.Sin(PhiLambda)
	S3 = S3 + 0.00129*math.Sin(3*(L3-L4))
	S3 = S3 + 0.00124*math.Sin(L1-L3)
	S3 = S3 - 0.00119*math.Sin(5*AnomSat-2*AnomJup+52.225/radcor)
	S3 = S3 + 0.00109*math.Sin(L1-L2)
	S3 = S3 - 0.00099*math.Sin(3*L3-7*L4+4*Pi4)
	S3 = S3 + 0.00091*math.Sin(W3-W4)
	S3 = S3 + 0.00081*math.Sin(3*L3-7*L4+Pi3+3*Pi4)
	S3 = S3 - 0.00076*math.Sin(2*L2-3*L3+Pi3)
	S3 = S3 + 0.00069*math.Sin(Pi4-LongPerJ)
	S3 = S3 - 0.00058*math.Sin(2*L3-3*L4+Pi4)
	S3 = S3 + 0.00057*math.Sin(L3+Pi3-2*LongPerJ-2*AnomJup)
	S3 = S3 - 0.00057*math.Sin(L3-2*L4+Pi4)
	S3 = S3 - 0.00052*math.Sin(Pi2-Pi3)
	S3 = S3 - 0.00052*math.Sin(L2-2*L3+Pi1)
	S3 = S3 + 0.00048*math.Sin(L3-2*L4+Pi3)
	S3 = S3 - 0.00045*math.Sin(2*L2-3*L3+Pi4)
	S3 = S3 - 0.00041*math.Sin(Pi2-Pi4)
	S3 = S3 - 0.00038*math.Sin(2*AnomJup)
	S3 = S3 - 0.00033*math.Sin(Pi3-Pi4+W3-W4)
	S3 = S3 - 0.00032*math.Sin(3*L3-7*L4+2*Pi3+2*Pi4)
	S3 = S3 + 0.00030*math.Sin(4*(L3-L4))
	S3 = S3 - 0.00029*math.Sin(W3+NodeJup-2*LongPerJ-2*AnomJup)
	S3 = S3 + 0.00029*math.Sin(L3+Pi4-2*LongPerJ-2*AnomJup)
	S3 = S3 + 0.00026*math.Sin(L3-LongPerJ-AnomJup)
	S3 = S3 + 0.00024*math.Sin(L2-3*L3+2*L4)
	S3 = S3 + 0.00021*math.Sin(2*(L3-LongPerJ-AnomJup))
	S3 = S3 - 0.00021*math.Sin(L3-Pi2)
	S3 = S3 + 0.00017*math.Sin(2*(L3-Pi3))

	S4 = 0.84109 * math.Sin(L4-Pi4)
	S4 = S4 + 0.03429*math.Sin(Pi4-Pi3)
	S4 = S4 - 0.03305*math.Sin(2*(NodeJup-LongPerJ))
	S4 = S4 - 0.03211*math.Sin(AnomJup)
	S4 = S4 - 0.01860*math.Sin(L4-Pi3)
	S4 = S4 + 0.01182*math.Sin(NodeJup-W4)
	S4 = S4 + 0.00622*math.Sin(L4+Pi4-2*AnomJup-2*LongPerJ)
	S4 = S4 + 0.00385*math.Sin(2*(L4-Pi4))
	S4 = S4 - 0.00284*math.Sin(5*AnomSat-2*AnomJup+52.225/radcor)
	S4 = S4 - 0.00233*math.Sin(2*(NodeJup-Pi4))
	S4 = S4 - 0.00223*math.Sin(L3-L4)
	S4 = S4 - 0.00208*math.Sin(L4-LongPerJ)
	S4 = S4 + 0.00177*math.Sin(NodeJup+W4-2*Pi4)
	S4 = S4 + 0.00134*math.Sin(Pi4-LongPerJ)
	S4 = S4 + 0.00125*math.Sin(2*(L4-AnomJup-LongPerJ))
	S4 = S4 - 0.00117*math.Sin(2*AnomJup)
	S4 = S4 - 0.00112*math.Sin(2*(L3-L4))
	S4 = S4 + 0.00106*math.Sin(3*L3-7*L4+4*Pi4)
	S4 = S4 + 0.00102*math.Sin(L4-AnomJup-LongPerJ)
	S4 = S4 + 0.00096*math.Sin(2*L4-NodeJup-W4)
	S4 = S4 + 0.00087*math.Sin(2*(NodeJup-W4))
	S4 = S4 - 0.00087*math.Sin(3*L3-7*L4+Pi3+3*Pi4)
	S4 = S4 + 0.00085*math.Sin(L3-2*L4+Pi4)
	S4 = S4 - 0.00081*math.Sin(2*(L4-NodeJup))
	S4 = S4 + 0.00071*math.Sin(L4+Pi4-2*LongPerJ-3*AnomJup)
	S4 = S4 + 0.00060*math.Sin(L1-L4)
	S4 = S4 - 0.00056*math.Sin(NodeJup-W3)
	S4 = S4 - 0.00055*math.Sin(L3-2*L4+Pi3)
	S4 = S4 + 0.00051*math.Sin(L2-L4)
	S4 = S4 + 0.00042*math.Sin(2*(NodeJup-AnomJup-LongPerJ))
	S4 = S4 + 0.00039*math.Sin(2*(Pi4-W4))
	S4 = S4 + 0.00036*math.Sin(NodeJup+LongPerJ-Pi4-W4)
	S4 = S4 + 0.00035*math.Sin(2*AnomSat-AnomJup+188.37/radcor)
	S4 = S4 - 0.00035*math.Sin(L4-Pi4+2*LongPerJ-2*NodeJup)
	S4 = S4 - 0.00032*math.Sin(L4+Pi4-2*LongPerJ-AnomJup)
	S4 = S4 + 0.00030*math.Sin(3*L3-7*L4+2*Pi3+2*Pi4)
	S4 = S4 + 0.00030*math.Sin(2*AnomSat-2*AnomJup+149.15/radcor)
	S4 = S4 + 0.00028*math.Sin(L4-Pi4+2*NodeJup-2*LongPerJ)
	S4 = S4 - 0.00028*math.Sin(2*(L4-W4))
	S4 = S4 - 0.00027*math.Sin(Pi3-Pi4+W3-W4)
	S4 = S4 - 0.00026*math.Sin(5*AnomSat-3*AnomJup+188.37/radcor)
	S4 = S4 + 0.00025*math.Sin(W4-W3)
	S4 = S4 - 0.00025*math.Sin(L2-3*L3+2*L4)
	S4 = S4 - 0.00023*math.Sin(3*(L3-L4))
	S4 = S4 + 0.00021*math.Sin(2*L4-2*LongPerJ-3*AnomJup)
	S4 = S4 - 0.00021*math.Sin(2*L3-3*L4+Pi4)
	S4 = S4 + 0.00019*math.Sin(L4-Pi4-AnomJup)
	S4 = S4 - 0.00019*math.Sin(2*L4-Pi3-Pi4)
	S4 = S4 - 0.00018*math.Sin(L4-Pi4+AnomJup)
	S4 = S4 - 0.00016*math.Sin(L4+Pi3-2*LongPerJ-2*AnomJup)

	S1 = S1 / radcor
	S2 = S2 / radcor
	S3 = S3 / radcor
	S4 = S4 / radcor

	TL1 = L1 + S1
	TL2 = L2 + S2
	TL3 = L3 + S3
	TL4 = L4 + S4

	B1 = 0.0006502 * math.Sin(TL1-W1)
	B1 = B1 + 0.0001835*math.Sin(TL1-W2)
	B1 = B1 + 0.0000329*math.Sin(TL1-W3)
	B1 = B1 - 0.0000311*math.Sin(TL1-NodeJup)
	B1 = B1 + 0.0000093*math.Sin(TL1-W4)
	B1 = B1 + 0.0000075*math.Sin(3*TL1-4*L2-1.9927/radcor*S1+W2)
	B1 = B1 + 0.0000046*math.Sin(TL1+NodeJup-2*LongPerJ-2*AnomJup)
	B1 = math.Atan(B1)

	B2 = 0.0081275 * math.Sin(TL2-W2)
	B2 = B2 + 0.0004512*math.Sin(TL2-W3)
	B2 = B2 - 0.0003286*math.Sin(TL2-NodeJup)
	B2 = B2 + 0.0001164*math.Sin(TL2-W4)
	B2 = B2 + 0.0000273*math.Sin(L1-2*L3+1.0146/radcor*S2+W2)
	B2 = B2 + 0.0000143*math.Sin(TL2+NodeJup-2*LongPerJ-2*AnomJup)
	B2 = B2 - 0.0000143*math.Sin(TL2-W1)
	B2 = B2 + 0.0000035*math.Sin(TL2-NodeJup+AnomJup)
	B2 = B2 - 0.0000028*math.Sin(L1-2*L3+1.0146/radcor*S2+W3)
	B2 = math.Atan(B2)

	B3 = 0.0032364 * math.Sin(TL3-W3)
	B3 = B3 - 0.0016911*math.Sin(TL3-NodeJup)
	B3 = B3 + 0.0006849*math.Sin(TL3-W4)
	B3 = B3 - 0.0002806*math.Sin(TL3-W2)
	B3 = B3 + 0.0000321*math.Sin(TL3+NodeJup-2*LongPerJ-2*AnomJup)
	B3 = B3 + 0.0000051*math.Sin(TL3-NodeJup+AnomJup)
	B3 = B3 - 0.0000045*math.Sin(TL3-NodeJup-AnomJup)
	B3 = B3 - 0.0000045*math.Sin(TL3+NodeJup-2*LongPerJ)
	B3 = B3 + 0.0000037*math.Sin(TL3+NodeJup-2*LongPerJ-3*AnomJup)
	B3 = B3 + 0.0000030*math.Sin(2*L2-3*TL3+4.03/radcor*S3+W2)
	B3 = B3 - 0.0000021*math.Sin(2*L2-3*TL3+4.03/radcor*S3+W3)
	B3 = math.Atan(B3)

	B4 = -0.0076579 * math.Sin(TL4-NodeJup)
	B4 = B4 + 0.0044148*math.Sin(TL4-W4)
	B4 = B4 - 0.0005106*math.Sin(TL4-W3)
	B4 = B4 + 0.0000773*math.Sin(TL4+NodeJup-2*LongPerJ-2*AnomJup)
	B4 = B4 + 0.0000104*math.Sin(TL4-NodeJup+AnomJup)
	B4 = B4 - 0.0000102*math.Sin(TL4-NodeJup-AnomJup)
	B4 = B4 + 0.0000088*math.Sin(TL4+NodeJup-2*LongPerJ-3*AnomJup)
	B4 = B4 - 0.0000038*math.Sin(TL4+NodeJup-2*LongPerJ-AnomJup)
	B4 = math.Atan(B4)

	R1 = -0.0041339 * math.Cos(2*(L1-L2))
	R1 = R1 - 0.0000395*math.Cos(L1-Pi3)
	R1 = R1 - 0.0000214*math.Cos(L1-Pi4)
	R1 = R1 + 0.0000170*math.Cos(L1-L2)
	R1 = R1 - 0.0000162*math.Cos(L1-Pi1)
	R1 = R1 - 0.0000130*math.Cos(4*(L1-L2))
	R1 = R1 + 0.0000106*math.Cos(L1-L3)
	R1 = R1 - 0.0000063*math.Cos(L1+Pi3-2*LongPerJ-2*AnomJup)

	R2 = 0.0093847 * math.Cos(L1-L2)
	R2 = R2 - 0.0003114*math.Cos(L2-Pi3)
	R2 = R2 - 0.0001738*math.Cos(L2-Pi4)
	R2 = R2 - 0.0000941*math.Cos(L2-Pi2)
	R2 = R2 + 0.0000553*math.Cos(L2-L3)
	R2 = R2 + 0.0000523*math.Cos(L1-L3)
	R2 = R2 - 0.0000290*math.Cos(2*(L1-L2))
	R2 = R2 + 0.0000166*math.Cos(2*(L2-W2))
	R2 = R2 + 0.0000107*math.Cos(L1-2*L3+Pi3)
	R2 = R2 - 0.0000102*math.Cos(L2-Pi1)
	R2 = R2 - 0.0000091*math.Cos(2*(L1-L3))

	R3 = -0.0014377 * math.Cos(L3-Pi3)
	R3 = R3 - 0.0007904*math.Cos(L3-Pi4)
	R3 = R3 + 0.0006342*math.Cos(L2-L3)
	R3 = R3 - 0.0001758*math.Cos(2*(L3-L4))
	R3 = R3 + 0.0000294*math.Cos(L3-L4)
	R3 = R3 - 0.0000156*math.Cos(3*(L3-L4))
	R3 = R3 + 0.0000155*math.Cos(L1-L3)
	R3 = R3 - 0.0000153*math.Cos(L1-L2)
	R3 = R3 + 0.0000070*math.Cos(2*L2-3*L3+Pi3)
	R3 = R3 - 0.0000051*math.Cos(L3+Pi3-2*LongPerJ-2*AnomJup)

	R4 = -0.0073391 * math.Cos(L4-Pi4)
	R4 = R4 + 0.0001620*math.Cos(L4-Pi3)
	R4 = R4 + 0.0000974*math.Cos(L3-L4)
	R4 = R4 - 0.0000541*math.Cos(L4+Pi4-2*LongPerJ-2*AnomJup)
	R4 = R4 - 0.0000269*math.Cos(2*(L4-Pi4))
	R4 = R4 + 0.0000182*math.Cos(L4-LongPerJ)
	R4 = R4 + 0.0000177*math.Cos(2*(L3-L4))
	R4 = R4 - 0.0000167*math.Cos(2*L4-NodeJup-W4)
	R4 = R4 + 0.0000167*math.Cos(NodeJup-W4)
	R4 = R4 - 0.0000155*math.Cos(2*(L4-LongPerJ-AnomJup))
	R4 = R4 + 0.0000142*math.Cos(2*(L4-NodeJup))
	R4 = R4 + 0.0000104*math.Cos(L1-L4)
	R4 = R4 + 0.0000092*math.Cos(L2-L4)
	R4 = R4 - 0.0000089*math.Cos(L4-LongPerJ-AnomJup)
	R4 = R4 - 0.0000062*math.Cos(L4+Pi4-2*LongPerJ-3*AnomJup)
	R4 = R4 + 0.0000048*math.Cos(2*(L4-W4))

	R1 = 5.90730 * (1 + R1)
	R2 = 9.39912 * (1 + R2)
	R3 = 14.99240 * (1 + R3)
	R4 = 26.36990 * (1 + R4)

	T0 = (AJD - 2433282.423) / 36525
	Precession = (1.3966626*T0 + 0.0003088*common.Sqr(T0)) / radcor

	TL1 = TL1 + Precession
	TL2 = TL2 + Precession
	TL3 = TL3 + Precession
	TL4 = TL4 + Precession
	NodeJup = NodeJup + Precession

	T0 = (AJD - stdate.AstJulianDatePrim(1900, 1, 1, 0)) / 36525
	Inclination = (3.120262 + 0.0006*T0) / radcor

	SatX[0] = R1 * math.Cos(TL1-NodeJup) * math.Cos(B1)
	SatY[0] = R1 * math.Sin(TL1-NodeJup) * math.Cos(B1)
	SatZ[0] = R1 * math.Sin(B1)

	SatX[1] = R2 * math.Cos(TL2-NodeJup) * math.Cos(B2)
	SatY[1] = R2 * math.Sin(TL2-NodeJup) * math.Cos(B2)
	SatZ[1] = R2 * math.Sin(B2)

	SatX[2] = R3 * math.Cos(TL3-NodeJup) * math.Cos(B3)
	SatY[2] = R3 * math.Sin(TL3-NodeJup) * math.Cos(B3)
	SatZ[2] = R3 * math.Sin(B3)

	SatX[3] = R4 * math.Cos(TL4-NodeJup) * math.Cos(B4)
	SatY[3] = R4 * math.Sin(TL4-NodeJup) * math.Cos(B4)
	SatZ[3] = R4 * math.Sin(B4)

	SatX[4] = 0
	SatY[4] = 0
	SatZ[4] = 1

	T0 = (AJD - 2451545.0) / 36525.0
	TD1 = 100.464441 + 1.0209550*T0 + 0.00040117*common.Sqr(T0) + 0.000000569*common.Sqr(T0)*T0
	TD1 = TD1 / radcor

	TD2 = 1.303270 - 0.0054966*T0 + 0.00000465*common.Sqr(T0) - 0.000000004*common.Sqr(T0)*T0
	TD2 = TD2 / radcor

	for i := 0; i < 5; i++ {
		Transforms[i].A[0] = SatX[i]
		Transforms[i].B[0] = SatY[i]*math.Cos(Inclination) - SatZ[i]*math.Sin(Inclination)
		Transforms[i].C[0] = SatY[i]*math.Sin(Inclination) + SatZ[i]*math.Cos(Inclination)

		Transforms[i].A[1] = Transforms[i].A[0]*math.Cos(NodeJup-TD1) - Transforms[i].B[0]*math.Sin(NodeJup-TD1)
		Transforms[i].B[1] = Transforms[i].A[0]*math.Sin(NodeJup-TD1) + Transforms[i].B[0]*math.Cos(NodeJup-TD1)
		Transforms[i].C[1] = Transforms[i].C[0]

		Transforms[i].A[2] = Transforms[i].A[1]
		Transforms[i].B[2] = Transforms[i].B[1]*math.Cos(TD2) - Transforms[i].C[1]*math.Sin(TD2)
		Transforms[i].C[2] = Transforms[i].B[1]*math.Sin(TD2) + Transforms[i].C[1]*math.Cos(TD2)

		Transforms[i].A[3] = Transforms[i].A[2]*math.Cos(TD1) - Transforms[i].B[2]*math.Sin(TD1)
		Transforms[i].B[3] = Transforms[i].A[2]*math.Sin(TD1) + Transforms[i].B[2]*math.Cos(TD1)
		Transforms[i].C[3] = Transforms[i].C[2]

		Transforms[i].A[4] = Transforms[i].A[3]*math.Sin(Jup1) - Transforms[i].B[3]*math.Cos(Jup1)
		Transforms[i].B[4] = Transforms[i].A[3]*math.Cos(Jup1) + Transforms[i].B[3]*math.Sin(Jup1)
		Transforms[i].C[4] = Transforms[i].C[3]

		Transforms[i].A[5] = Transforms[i].A[4]
		Transforms[i].B[5] = Transforms[i].C[4]*math.Sin(Jup2) + Transforms[i].B[4]*math.Cos(Jup2)
		Transforms[i].C[5] = Transforms[i].C[4]*math.Cos(Jup2) - Transforms[i].B[4]*math.Sin(Jup2)
	}

	Angle = math.Atan2(Transforms[4].C[5], Transforms[4].A[5])

	// {Io calculations}
	Result.Io.X = Transforms[0].A[5]*math.Cos(Angle) - Transforms[0].C[5]*math.Sin(Angle)
	Result.Io.Y = Transforms[0].A[5]*math.Sin(Angle) + Transforms[0].C[5]*math.Cos(Angle)
	TD1 = Transforms[0].B[5]

	// {correct for light time}
	TD2 = math.Abs(TD1) / 17295 * math.Sqrt(1-common.Sqr(Result.Io.X/R1))
	Result.Io.X = Result.Io.X + TD2

	// {correct for perspective}
	TD2 = EJDist / (EJDist + TD1/2095)
	Result.Io.X = Result.Io.X * TD2
	Result.Io.Y = Result.Io.Y * TD2

	// {Europa calculations}
	Result.Europa.X = Transforms[1].A[5]*math.Cos(Angle) - Transforms[1].C[5]*math.Sin(Angle)
	Result.Europa.Y = Transforms[1].A[5]*math.Sin(Angle) + Transforms[1].C[5]*math.Cos(Angle)
	TD1 = Transforms[1].B[5]

	// {correct for light time}
	TD2 = math.Abs(TD1) / 21819 * math.Sqrt(1-common.Sqr(Result.Europa.X/R2))
	Result.Europa.X = Result.Europa.X + TD2

	// {correct for perspective}
	TD2 = EJDist / (EJDist + TD1/2095)
	Result.Europa.X = Result.Europa.X * TD2
	Result.Europa.Y = Result.Europa.Y * TD2

	// {Ganymede calculations}
	Result.Ganymede.X = Transforms[2].A[5]*math.Cos(Angle) - Transforms[2].C[5]*math.Sin(Angle)
	Result.Ganymede.Y = Transforms[2].A[5]*math.Sin(Angle) + Transforms[2].C[5]*math.Cos(Angle)
	TD1 = Transforms[2].B[5]

	// {correct for light time}
	TD2 = math.Abs(TD1) / 27558 * math.Sqrt(1-common.Sqr(Result.Ganymede.X/R3))
	Result.Ganymede.X = Result.Ganymede.X + TD2

	// {correct for perspective}
	TD2 = EJDist / (EJDist + TD1/2095)
	Result.Ganymede.X = Result.Ganymede.X * TD2
	Result.Ganymede.Y = Result.Ganymede.Y * TD2

	// {Callisto calculations}
	Result.Callisto.X = Transforms[3].A[5]*math.Cos(Angle) - Transforms[3].C[5]*math.Sin(Angle)
	Result.Callisto.Y = Transforms[3].A[5]*math.Sin(Angle) + Transforms[3].C[5]*math.Cos(Angle)
	TD1 = Transforms[3].B[5]

	// {correct for light time}
	TD2 = math.Abs(TD1) / 36548 * math.Sqrt(1-common.Sqr(Result.Callisto.X/R4))
	Result.Callisto.X = Result.Callisto.X + TD2

	// {correct for perspective}
	TD2 = EJDist / (EJDist + TD1/2095)
	Result.Callisto.X = Result.Callisto.X * TD2
	Result.Callisto.Y = Result.Callisto.Y * TD2
	return Result
}

// GetJupSats
func GetJupSats(JD delphi.TDateTime, HighPrecision, Shadows bool) TStJupSats {
	if HighPrecision {
		return _JupSatsHi(JD, Shadows)
	}
	return _JupSatsLo(JD)
}
