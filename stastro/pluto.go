package stastro

import (
	"math"
)

// ComputePluto
func ComputePluto(JD float64) TStEclipticalCord {
	var Result TStEclipticalCord

	T := (JD - 2451545.0) / 36525.0
	J := (34.35 + 3034.9057*T) / radcor
	S := (50.08 + 1222.1138*T) / radcor
	P := (238.96 + 144.9600*T) / radcor

	L := -19798886 * math.Sin(P)
	L += 19848454 * math.Cos(P)
	L += 897499 * math.Sin(2*P)
	L -= 4955707 * math.Cos(2*P)
	L += 610820 * math.Sin(3*P)
	L += 1210521 * math.Cos(3*P)
	L -= 341639 * math.Sin(4*P)
	L -= 189719 * math.Cos(4*P)
	L += 129027 * math.Sin(5*P)
	L -= 34863 * math.Cos(5*P)
	L -= 38215 * math.Sin(6*P)
	L += 31061 * math.Cos(6*P)
	L += 20349 * math.Sin(S-P)
	L -= 9886 * math.Cos(S-P)
	L -= 4045 * math.Sin(S)
	L -= 4904 * math.Cos(S)
	L -= 5885 * math.Sin(S+P)
	L -= 3238 * math.Cos(S+P)
	L -= 3812 * math.Sin(S+2*P)
	L += 3011 * math.Cos(S+2*P)
	L -= 601 * math.Sin(S+3*P)
	L += 3468 * math.Cos(S+3*P)
	L += 1237 * math.Sin(2*(S-P))
	L += 463 * math.Cos(2*(S-P))
	L += 1086 * math.Sin(2*S-P)
	L -= 911 * math.Cos(2*S-P)
	L += 595 * math.Sin(2*S)
	L -= 1229 * math.Cos(2*S)
	L += 2484 * math.Sin(J-S)
	L -= 485 * math.Cos(J-S)
	L += 839 * math.Sin(J-S+P)
	L -= 1414 * math.Cos(J-S+P)
	L -= 964 * math.Sin(J-3*P)
	L += 1059 * math.Cos(J-3*P)
	L -= 2303 * math.Sin(J-2*P)
	L -= 1038 * math.Cos(J-2*P)
	L += 7049 * math.Sin(J-P)
	L += 747 * math.Cos(J-P)
	L += 1179 * math.Sin(J)
	L -= 358 * math.Cos(J)
	L += 393 * math.Sin(J+P)
	L -= 63 * math.Cos(J+P)
	L += 111 * math.Sin(J+2*P)
	L -= 268 * math.Cos(J+2*P)
	L -= 52 * math.Sin(J+3*P)
	L -= 154 * math.Cos(J+3*P)
	L -= 78 * math.Sin(J+4*P)
	L -= 30 * math.Cos(J+4*P)
	L -= 34 * math.Sin(J+S-3*P)
	L -= 26 * math.Cos(J+S-3*P)
	L -= 43 * math.Sin(J+S-2*P)
	L += 1 * math.Cos(J+S-2*P)
	L -= 15 * math.Sin(J+S-P)
	L += 21 * math.Cos(J+S-P)
	L -= 1 * math.Sin(J+S)
	L += 15 * math.Cos(J+S)
	L += 4 * math.Sin(J+S+P)
	L += 7 * math.Cos(J+S+P)
	L += 1 * math.Sin(J+S+3*P)
	L += 5 * math.Cos(J+S+3*P)
	L += 8 * math.Sin(2*J-6*P)
	L += 3 * math.Cos(2*J-6*P)
	L -= 3 * math.Sin(2*J-5*P)
	L += 6 * math.Cos(2*J-5*P)
	L += 6 * math.Sin(2*J-4*P)
	L -= 13 * math.Cos(2*J-4*P)
	L += 10 * math.Sin(2*J-3*P)
	L += 22 * math.Cos(2*J-3*P)
	L -= 57 * math.Sin(2*J-2*P)
	L -= 32 * math.Cos(2*J-2*P)
	L += 157 * math.Sin(2*J-P)
	L -= 46 * math.Cos(2*J-P)
	L += 12 * math.Sin(2*J)
	L -= 18 * math.Cos(2*J)
	L -= 4 * math.Sin(2*J-P)
	L += 8 * math.Cos(2*J-P)
	L -= 5 * math.Sin(2*(J+P))
	L += 0 * math.Sin(2*(J+P))
	L += 3 * math.Sin(2*J+3*P)
	L += 4 * math.Cos(2*J+3*P)
	L -= 1 * math.Sin(3*J-2*P)
	L -= 1 * math.Cos(3*J-2*P)
	L += 6 * math.Sin(3*J-P)
	L -= 3 * math.Cos(3*J-P)
	L -= 1 * math.Sin(3*J)
	L -= 2 * math.Cos(3*J)
	Result.L0 = (238.956785 + 144.96*T + (L / 1000000)) / radcor

	B := -5453098 * math.Sin(P)
	B -= 14974876 * math.Cos(P)
	B += 3527363 * math.Sin(2*P)
	B += 1672673 * math.Cos(2*P)
	B -= 1050939 * math.Sin(3*P)
	B += 327763 * math.Cos(3*P)
	B += 178691 * math.Sin(4*P)
	B -= 291925 * math.Cos(4*P)
	B += 18763 * math.Sin(5*P)
	B += 100448 * math.Cos(5*P)
	B -= 30594 * math.Sin(6*P)
	B -= 25838 * math.Cos(6*P)
	B += 4965 * math.Sin(S-P)
	B += 11263 * math.Cos(S-P)
	B += 310 * math.Sin(S)
	B -= 132 * math.Cos(S)
	B += 2036 * math.Sin(S+P)
	B -= 947 * math.Cos(S+P)
	B -= 2 * math.Sin(S+2*P)
	B -= 674 * math.Cos(S+2*P)
	B -= 329 * math.Sin(S+3*P)
	B -= 563 * math.Cos(S+3*P)
	B -= 64 * math.Sin(2*(S-P))
	B += 39 * math.Cos(2*(S-P))
	B -= 94 * math.Sin(2*S-P)
	B += 210 * math.Cos(2*S-P)
	B -= 8 * math.Sin(2*S)
	B -= 160 * math.Cos(2*S)
	B += 177 * math.Sin(J-S)
	B += 259 * math.Cos(J-S)
	B += 17 * math.Sin(J-S+P)
	B += 234 * math.Cos(J-S+P)
	B += 582 * math.Sin(J-3*P)
	B -= 285 * math.Cos(J-3*P)
	B -= 298 * math.Sin(J-2*P)
	B += 692 * math.Cos(J-2*P)
	B += 157 * math.Sin(J-P)
	B += 201 * math.Cos(J-P)
	B += 304 * math.Sin(J)
	B += 825 * math.Cos(J)
	B -= 124 * math.Sin(J+P)
	B -= 29 * math.Cos(J+P)
	B += 15 * math.Sin(J+2*P)
	B += 8 * math.Cos(J+2*P)
	B += 7 * math.Sin(J+3*P)
	B += 15 * math.Cos(J+3*P)
	B += 2 * math.Sin(J+4*P)
	B += 2 * math.Cos(J+4*P)
	B += 4 * math.Sin(J+S-3*P)
	B += 2 * math.Cos(J+S-3*P)
	B += 3 * math.Sin(J+S-2*P)
	B += 0 * math.Cos(J+S-2*P)
	B += 1 * math.Sin(J+S-P)
	B -= 1 * math.Cos(J+S-P)
	B += 0 * math.Sin(J+S)
	B -= 2 * math.Cos(J+S)
	B += 1 * math.Sin(J+S+P)
	B -= 0 * math.Cos(J+S+P)
	B += 1 * math.Sin(J+S+3*P)
	B -= 1 * math.Cos(J+S+3*P)
	B -= 2 * math.Sin(2*J-6*P)
	B -= 3 * math.Cos(2*J-6*P)
	B += 1 * math.Sin(2*J-5*P)
	B += 2 * math.Cos(2*J-5*P)
	B -= 8 * math.Sin(2*J-4*P)
	B += 2 * math.Cos(2*J-4*P)
	B += 10 * math.Sin(2*J-3*P)
	B -= 7 * math.Cos(2*J-3*P)
	B += 0 * math.Sin(2*J-2*P)
	B += 21 * math.Cos(2*J-2*P)
	B += 8 * math.Sin(2*J-P)
	B += 5 * math.Cos(2*J-P)
	B += 13 * math.Sin(2*J)
	B += 16 * math.Cos(2*J)
	B -= 2 * math.Sin(2*J-P)
	B -= 3 * math.Cos(2*J-P)
	B += 0 * math.Sin(2*(J+P))
	B += 0 * math.Cos(2*(J+P))
	B += 0 * math.Sin(2*J+3*P)
	B += 1 * math.Cos(2*J+3*P)
	B += 0 * math.Sin(3*J-2*P)
	B += 1 * math.Cos(3*J-2*P)
	B += 0 * math.Sin(3*J-P)
	B += 0 * math.Cos(3*J-P)
	B += 0 * math.Sin(3*J)
	B += 1 * math.Cos(3*J)
	Result.B0 = (-3.908202 + B/1000000) / radcor

	R := 66867334 * math.Sin(P)
	R += 68955876 * math.Cos(P)
	R -= 11826086 * math.Sin(2*P)
	R -= 333765 * math.Cos(2*P)
	R += 1593657 * math.Sin(3*P)
	R -= 1439953 * math.Cos(3*P)
	R -= 18948 * math.Sin(4*P)
	R += 482443 * math.Cos(4*P)
	R -= 66634 * math.Sin(5*P)
	R -= 85576 * math.Cos(5*P)
	R += 30841 * math.Sin(6*P)
	R -= 5765 * math.Cos(6*P)
	R -= 6140 * math.Sin(S-P)
	R += 22254 * math.Cos(S-P)
	R += 4434 * math.Sin(S)
	R += 4443 * math.Cos(S)
	R -= 1518 * math.Sin(S+P)
	R += 641 * math.Cos(S+P)
	R -= 5 * math.Sin(S+2*P)
	R += 792 * math.Cos(S+2*P)
	R += 518 * math.Sin(S+3*P)
	R += 518 * math.Cos(S+3*P)
	R -= 13 * math.Sin(2*(S-P))
	R -= 221 * math.Cos(2*(S-P))
	R += 837 * math.Sin(2*S-P)
	R -= 494 * math.Cos(2*S-P)
	R -= 281 * math.Sin(2*S)
	R += 616 * math.Cos(2*S)
	R += 260 * math.Sin(J-S)
	R -= 395 * math.Cos(J-S)
	R -= 191 * math.Sin(J-S+P)
	R -= 396 * math.Cos(J-S+P)
	R -= 3218 * math.Sin(J-3*P)
	R += 370 * math.Cos(J-3*P)
	R += 8019 * math.Sin(J-2*P)
	R -= 7689 * math.Cos(J-2*P)
	R += 105 * math.Sin(J-P)
	R += 45637 * math.Cos(J-P)
	R += 8623 * math.Sin(J)
	R += 8444 * math.Cos(J)
	R -= 896 * math.Sin(J+P)
	R -= 801 * math.Cos(J+P)
	R += 208 * math.Sin(J+2*P)
	R -= 122 * math.Cos(J+2*P)
	R -= 133 * math.Sin(J+3*P)
	R += 65 * math.Cos(J+3*P)
	R -= 16 * math.Sin(J+4*P)
	R += 1 * math.Cos(J+4*P)
	R -= 22 * math.Sin(J+S-3*P)
	R += 7 * math.Cos(J+S-3*P)
	R -= 8 * math.Sin(J+S-2*P)
	R += 16 * math.Cos(J+S-2*P)
	R += 2 * math.Sin(J+S-P)
	R += 9 * math.Cos(J+S-P)
	R += 12 * math.Sin(J+S)
	R += 5 * math.Cos(J+S)
	R += 1 * math.Sin(J+S+P)
	R -= 3 * math.Cos(J+S+P)
	R += 1 * math.Sin(J+S+3*P)
	R += 0 * math.Cos(J+S+3*P)
	R += 9 * math.Sin(2*J-6*P)
	R += 5 * math.Cos(2*J-6*P)
	R += 2 * math.Sin(2*J-5*P)
	R -= 1 * math.Cos(2*J-5*P)
	R += 14 * math.Sin(2*J-4*P)
	R += 10 * math.Cos(2*J-4*P)
	R -= 65 * math.Sin(2*J-3*P)
	R += 12 * math.Cos(2*J-3*P)
	R += 126 * math.Sin(2*J-2*P)
	R -= 233 * math.Cos(2*J-2*P)
	R += 270 * math.Sin(2*J-P)
	R += 1068 * math.Cos(2*J-P)
	R += 254 * math.Sin(2*J)
	R += 155 * math.Cos(2*J)
	R -= 26 * math.Sin(2*J-P)
	R -= 2 * math.Cos(2*J-P)
	R += 7 * math.Sin(2*(J+P))
	R += 0 * math.Cos(2*(J+P))
	R -= 11 * math.Sin(2*J+3*P)
	R += 4 * math.Cos(2*J+3*P)
	R += 4 * math.Sin(3*J-2*P)
	R -= 14 * math.Cos(3*J-2*P)
	R += 18 * math.Sin(3*J-P)
	R += 35 * math.Cos(3*J-P)
	R += 13 * math.Sin(3*J)
	R += 3 * math.Cos(3*J)
	Result.R0 = 40.7247248 + R/10000000

	return Result
} // ComputePluto
