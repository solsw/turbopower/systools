package stastro

import (
	"math"

	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

// _RiseSetPrim {-primitive routine for finding rise/set times}
func _RiseSetPrim(LD stdate.TStDate, Longitude, Latitude, H0 float64, PR TStPosRecArray, ApproxOnly bool) TStRiseSetRec {
	var (
		ST, NST, HA, LatR, N1, N2, N3, TTran, TRise, TSet, TV1, TV2, A1, A2, DeltaR, DeltaS, RA, DC, Alt float64

		ICount int16
		UT     stdate.TStDateTimeRec
		Result TStRiseSetRec
	)
	H0 = H0 / radcor
	UT.D = LD
	UT.T = 0
	ST = SiderealTime(UT)
	LatR = Latitude / radcor
	// {check if object never rises/sets}
	N1 = math.Sin(H0) - math.Sin(LatR)*math.Sin(PR[1].DC/radcor)
	N2 = math.Cos(LatR) * math.Cos(PR[1].DC/radcor)

	HA = N1 / N2
	if math.Abs(HA) >= 1 {
		// {    if ((Latitude - 90) >= 90) then begin}
		if HA > 0 {
			// {object never rises}
			Result.ORise = -2
			Result.OSet = -2
		} else {
			// {object never sets, i.e., it is circumpolar}
			Result.ORise = -3
			Result.OSet = -3
		}
		return Result
	}

	HA = math.Acos(HA) * radcor
	if HA > 180 {
		HA = HA - 180
	}
	if HA < 0 {
		HA = HA + 180
	}

	// {compute approximate hour angle at transit}
	TTran = (PR[1].RA - Longitude - ST) / 360
	if math.Abs(TTran) >= 1 {
		TTran = Frac(TTran)
	}
	if TTran < 0 {
		TTran = TTran + 1
	}

	TRise = TTran - HA/360
	TSet = TTran + HA/360
	if math.Abs(TRise) >= 1 {
		TRise = Frac(TRise)
	}
	if TRise < 0 {
		TRise = TRise + 1
	}

	if math.Abs(TSet) >= 1 {
		TSet = Frac(TSet)
	}
	if TSet < 0 {
		TSet = TSet + 1
	}

	if !ApproxOnly {
		// {refine rise time by interpolation/iteration}
		ICount = 0
		TV1 = 0
		A1 = 0
		for {
			NST = ST + 360.985647*TRise
			NST = Frac(NST/360.0) * 360
			N1 = PR[1].RA - PR[0].RA
			N2 = PR[2].RA - PR[1].RA
			N3 = N2 - N1
			RA = PR[1].RA + TRise/2*(N1+N2+TRise*N3)

			N1 = PR[1].DC - PR[0].DC
			N2 = PR[2].DC - PR[1].DC
			N3 = N2 - N1
			DC = PR[1].DC + TRise/2*(N1+N2+TRise*N3)
			DC = DC / radcor

			HA = (NST + Longitude - RA) / radcor
			Alt = math.Asin(math.Sin(LatR)*math.Sin(DC) + math.Cos(LatR)*math.Cos(DC)*math.Cos(HA))
			DeltaR = ((Alt - H0) * radcor) / (360 * math.Cos(DC) * math.Cos(LatR) * math.Sin(HA))
			TRise = TRise + DeltaR
			ICount++
			if (ICount > 3) && (math.Abs(DeltaR) >= 0.0005) {
				if ICount == 4 {
					TV1 = TRise
					A1 = (Alt - H0) * radcor
				} else if ICount == 5 {
					TV2 = TRise
					A2 = (Alt - H0) * radcor
					TRise = TV1 + (A1/A2)*(TV1-TV2)
					break
				}
			}
			if math.Abs(DeltaR) < 0.0005 { // {0.0005d = 0.72 min}
				break
			}
		}

		// {refine set time by interpolation/iteration}
		ICount = 0
		TV1 = 0
		A1 = 0
		for {
			NST = ST + 360.985647*TSet
			NST = Frac(NST/360.0) * 360
			N1 = PR[1].RA - PR[0].RA
			N2 = PR[2].RA - PR[1].RA
			N3 = N2 - N1
			RA = PR[1].RA + TSet/2*(N1+N2+TSet*N3)

			N1 = PR[1].DC - PR[0].DC
			N2 = PR[2].DC - PR[1].DC
			N3 = N2 - N1
			DC = PR[1].DC + TSet/2*(N1+N2+TSet*N3)
			DC = DC / radcor

			HA = (NST + Longitude - RA) / radcor
			Alt = math.Asin(math.Sin(LatR)*math.Sin(DC) + math.Cos(LatR)*math.Cos(DC)*math.Cos(HA))
			DeltaS = ((Alt - H0) * radcor) / (360 * math.Cos(DC) * math.Cos(LatR) * math.Sin(HA))
			TSet = TSet + DeltaS
			ICount++
			if (ICount > 3) && (math.Abs(DeltaS) >= 0.0005) {
				if ICount == 4 {
					TV1 = TSet
					A1 = (Alt - H0) * radcor
				} else if ICount == 5 {
					TV2 = TSet
					A2 = (Alt - H0) * radcor
					TSet = TV1 + (A1/A2)*(TV1-TV2)
					break
				}
			}
			if math.Abs(DeltaS) < 0.0005 { // {0.0005d = 0.72 min}
				break
			}
		}
	}

	if (TRise >= 0) && (TRise < 1) {
		Result.ORise = common.Trunc(TRise * stdate.SecondsInDay)
	} else {
		if TRise < 0 {
			Result.ORise = common.Trunc((TRise + 1) * stdate.SecondsInDay)
		} else {
			Result.ORise = common.Trunc(Frac(TRise) * stdate.SecondsInDay)
		}
	}
	if Result.ORise < 0 {
		Result.ORise += stdate.SecondsInDay
	}
	if Result.ORise >= stdate.SecondsInDay {
		Result.ORise -= stdate.SecondsInDay
	}

	if (TSet >= 0) && (TSet < 1) {
		Result.OSet = common.Trunc(TSet * stdate.SecondsInDay)
	} else {
		if TSet < 0 {
			Result.OSet = common.Trunc((TSet + 1) * stdate.SecondsInDay)
		} else {
			Result.OSet = common.Trunc(Frac(TSet) * stdate.SecondsInDay)
		}
	}
	if Result.OSet < 0 {
		Result.OSet += stdate.SecondsInDay
	}
	if Result.OSet >= stdate.SecondsInDay {
		Result.OSet -= stdate.SecondsInDay
	}
	return Result
} // _RiseSetPrim

// FixedRiseSet {-compute the rise/set time for a fixed object, e.g., star}
// {the value for H0 accounts for approximate refraction of 0.5667 deg.}
// {this routine does not refine the intial estimate and so may be off by five}
// {minutes or so}
func FixedRiseSet(LD stdate.TStDate, RA, DC, Longitude, Latitude float64) TStRiseSetRec {
	var (
		H0     float64
		UT     stdate.TStDateTimeRec
		RP     TStPosRecArray
		Result TStRiseSetRec
	)
	H0 = -0.5667
	UT.T = 0
	UT.D = LD

	if !CheckDate(UT) {
		Result.ORise = -4
		Result.OSet = -4
		return Result
	}

	RP[1].RA = RA
	RP[1].DC = DC
	Result = _RiseSetPrim(LD, Longitude, Latitude, H0, RP, true)
	return Result
}

// Twilight {-compute the beginning or end of twilight}
// {twilight computations are based on the zenith distance of the center }
// {of the solar disc.}
// {Civil = 6 deg. below the horizon}
// {Nautical = 12 deg. below the horizon}
// {Astronomical = 18 deg. below the horizon}
func Twilight(LD stdate.TStDate, Longitude, Latitude float64, TwiType TStTwilight) TStRiseSetRec {
	var (
		H0     float64
		UT     stdate.TStDateTimeRec
		RP     TStPosRecArray
		Result TStRiseSetRec
	)
	UT.D = LD - 1
	UT.T = 0

	if CheckDate(UT) {
		UT.D = UT.D + 2
		if !CheckDate(UT) {
			Result.ORise = -4
			Result.OSet = -4
			return Result
		}
		UT.D = UT.D - 2
	} else {
		Result.ORise = -4
		Result.OSet = -4
		return Result
	}

	switch TwiType {
	case ttCivil:
		H0 = -6.0
	case ttNautical:
		H0 = -12.0
	case ttAstronomical:
		H0 = -18.0
	default:
		H0 = -18.0
	}

	for i := 0; i < 3; i++ {
		UT.D = LD + i
		RP[i] = SunPos(UT)
		if i > 0 {
			if RP[i].RA < RP[i-1].RA {
				RP[i].RA = RP[i].RA + 360.0
			}
		}
	}
	Result = _RiseSetPrim(LD, Longitude, Latitude, H0, RP, false)
	return Result
} // Twilight

// _SolEqPrim {primitive routine for finding equinoxes and solstices}
func _SolEqPrim(Y int, K byte) stdate.TStDateTimeRec {
	var (
		JD, TD, LY, JCent, MA, SLong float64
		Result                       stdate.TStDateTimeRec
	)
	JD = 0
	Result.D = stdate.BadDate
	Result.T = stdate.BadTime

	// {the following algorithm is valid only in the range of [1000..3000 AD]}
	// {but is limited to returning dates in [MinYear..MaxYear]}
	if (Y < int(stdate.MinYear)) || (Y > int(stdate.MaxYear)) {
		return Result
	}

	// {compute approximate date/time for specified event}
	LY = (float64(Y) - 2000) / 1000
	switch K {
	case 0:
		JD = 2451623.80984 + 365242.37404*LY + 0.05169*common.Sqr(LY) - 0.00411*LY*common.Sqr(LY) - 0.00057*common.Sqr(common.Sqr(LY))
	case 1:
		JD = 2451716.56767 + 365241.62603*LY + 0.00325*common.Sqr(LY) + 0.00888*LY*common.Sqr(LY) - 0.00030*common.Sqr(common.Sqr(LY))
	case 2:
		JD = 2451810.21715 + 365242.01767*LY - 0.11575*common.Sqr(LY) + 0.00337*common.Sqr(LY)*LY + 0.00078*common.Sqr(common.Sqr(LY))
	case 3:
		JD = 2451900.05952 + 365242.74049*LY - 0.06223*common.Sqr(LY) - 0.00823*LY*common.Sqr(LY) + 0.00032*common.Sqr(common.Sqr(LY))
	}

	// {refine date/time by computing corrections due to solar longitude,}
	// {nutation and abberation. Iterate using the corrected time until}
	// {correction is less than one minute}
	for {
		Result.D = stdate.AstJulianDateToStDate(JD, true)
		Result.T = common.Trunc((Frac(JD) + 0.5) * 86400)
		if Result.T >= stdate.SecondsInDay {
			Result.T -= stdate.SecondsInDay
		}
		JCent = (JD - 2451545.0) / 36525.0

		// {approximate solar longitude - no FK5 correction}
		SLong = 280.46645 + 36000.76983*JCent + 0.0003032*common.Sqr(JCent)
		SLong = Frac((SLong)/360.0) * 360.0
		if SLong < 0 {
			SLong = SLong + 360
		}

		// {Equation of the center correction}
		MA = 357.52910 + 35999.05030*JCent
		MA = MA / radcor
		SLong += (1.914600 - 0.004817*JCent - 0.000014*common.Sqr(JCent)) * math.Sin(MA)
		SLong += (0.019993 - 0.000101*JCent) * math.Sin(2*MA)

		// {approximate nutation}
		TD = 125.04452 - 1934.136261*JCent + 0.0020708*common.Sqr(JCent)
		TD = TD / radcor
		TD = (-17.20*math.Sin(TD) - 1.32*math.Sin(2*SLong/radcor)) / 3600

		// {approximate abberation - solar distance is assumed to be 1 A.U.}
		SLong = SLong - (20.4989 / 3600) + TD

		// {correction to compute Julian Date for event}
		TD = 58 * math.Sin((float64(K)*90-SLong)/radcor)
		if math.Abs(TD) >= 0.0005 {
			JD = JD + TD
		}
		if math.Abs(TD) < 0.0005 {
			break
		}
	}
	return Result
} // _SolEqPrim

// Solstice {-compute the date/time of the summer or winter solstice}
// {if Summer = True,  compute astronomical summer solstice (summer in N. Hem.)}
// {          = False, compute astronomical winter solstice (winter in N. Hem.)}
func Solstice(Y, Epoch int, Summer bool) stdate.TStDateTimeRec {
	Y = CheckYear(Y, Epoch)
	if Summer {
		return _SolEqPrim(Y, 1)
	}
	return _SolEqPrim(Y, 3)
}

// Equinox {-compute the date/time of the vernal/autumnal equinox}
// {if Vernal = True,  compute astronomical vernal equinox (spring in N. Hem.)}
// {          = False, compute astronomical autumnal equinox (fall in N. Hem.)}
func Equinox(Y, Epoch int, Vernal bool) stdate.TStDateTimeRec {
	Y = CheckYear(Y, Epoch)
	if Vernal {
		return _SolEqPrim(Y, 0)
	}
	return _SolEqPrim(Y, 2)
}

// Easter {-compute the date of Easter}
func Easter(Y, Epoch int) stdate.TStDate {
	Y = CheckYear(Y, Epoch)
	if (Y < stdate.MinYear) || (Y > stdate.MaxYear) {
		return stdate.BadDate
	}

	A := Y % 19
	B := Y / 100
	C := Y % 100
	D := B / 4
	E := B % 4
	F := (B + 8) / 25
	G := (B - F + 1) / 3
	H := (19*A + B - D - G + 15) % 30
	I := C / 4
	K := C % 4
	L := (32 + 2*E + 2*I - H - K) % 7
	M := (A + 11*H + 22*L) / 451
	N := (H + L - 7*M + 114) / 31
	P := (H+L-7*M+114)%31 + 1

	return stdate.DMYToStDate(P, N, Y, Epoch)
}
