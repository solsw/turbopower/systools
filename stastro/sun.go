package stastro

import (
	"math"

	"gitlab.com/solsw/TurboPower/SysTools/internal/common"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

// SunPosPrim {-compute J2000 XYZ coordinates of the Sun}
func SunPosPrim(UT stdate.TStDateTimeRec) TStSunXYZRec {
	if !CheckDate(UT) {
		return TStSunXYZRec{SunX: -99, SunY: -99, SunZ: -99, RV: -99, SLong: -99, SLat: -99}
	}
	var JD, T0, A, L, B, X, Y, Z float64
	JD = stdate.AstJulianDate(UT.D) + float64(UT.T)/86400
	T0 = (JD - StdDate) / 365250

	// {solar longitude}
	L = 175347046.0
	L += 3341656.0 * math.Cos(4.6692568+6283.07585*T0)
	L += 34894 * math.Cos(4.6261000+12566.1517*T0)
	L += 3497 * math.Cos(2.7441000+5753.3849*T0)
	L += 3418 * math.Cos(2.8289000+3.5231*T0)
	L += 3136 * math.Cos(3.6277000+77713.7715*T0)
	L += 2676 * math.Cos(4.4181000+7860.4194*T0)
	L += 2343 * math.Cos(6.1352000+3930.2097*T0)
	L += 1324 * math.Cos(0.7425000+11506.7698*T0)
	L += 1273 * math.Cos(2.0371000+529.6910*T0)
	L += 1199 * math.Cos(1.1096000+1577.3435*T0)
	L += 990 * math.Cos(5.2330000+5884.9270*T0)
	L += 902 * math.Cos(2.0450000+26.1490*T0)
	L += 857 * math.Cos(3.5080000+398.149*T0)
	L += 780 * math.Cos(1.1790000+5223.694*T0)
	L += 753 * math.Cos(2.5330000+5507.553*T0)
	L += 505 * math.Cos(4.5830000+18849.228*T0)
	L += 492 * math.Cos(4.2050000+775.523*T0)
	L += 357 * math.Cos(2.9200000+0.067*T0)
	L += 317 * math.Cos(5.8490000+11790.626*T0)
	L += 284 * math.Cos(1.8990000+796.298*T0)
	L += 271 * math.Cos(0.3150000+10977.079*T0)
	L += 243 * math.Cos(0.3450000+5486.778*T0)
	L += 206 * math.Cos(4.8060000+2544.314*T0)
	L += 205 * math.Cos(1.8690000+5573.143*T0)
	L += 202 * math.Cos(2.4580000+6069.777*T0)
	L += 156 * math.Cos(0.8330000+213.299*T0)
	L += 132 * math.Cos(3.4110000+2942.463*T0)
	L += 126 * math.Cos(1.0830000+20.775*T0)
	L += 115 * math.Cos(0.6450000+0.980*T0)
	L += 103 * math.Cos(0.6360000+4694.003*T0)
	L += 102 * math.Cos(0.9760000+15720.839*T0)
	L += 102 * math.Cos(4.2670000+7.114*T0)
	L += 99 * math.Cos(6.2100000+2146.170*T0)
	L += 98 * math.Cos(0.6800000+155.420*T0)
	L += 86 * math.Cos(5.9800000+161000.690*T0)
	L += 85 * math.Cos(1.3000000+6275.960*T0)
	L += 85 * math.Cos(3.6700000+71430.700*T0)
	L += 80 * math.Cos(1.8100000+17260.150*T0)

	A = 628307584999.0
	A += 206059 * math.Cos(2.678235+6283.07585*T0)
	A += 4303 * math.Cos(2.635100+12566.1517*T0)
	A += 425 * math.Cos(1.590000+3.523*T0)
	A += 119 * math.Cos(5.796000+26.298*T0)
	A += 109 * math.Cos(2.966000+1577.344*T0)
	A += 93 * math.Cos(2.590000+18849.23*T0)
	A += 72 * math.Cos(1.140000+529.69*T0)
	A += 68 * math.Cos(1.870000+398.15*T0)
	A += 67 * math.Cos(4.410000+5507.55*T0)
	A += 59 * math.Cos(2.890000+5223.69*T0)
	A += 56 * math.Cos(2.170000+155.42*T0)
	A += 45 * math.Cos(0.400000+796.30*T0)
	A += 36 * math.Cos(0.470000+775.52*T0)
	A += 29 * math.Cos(2.650000+7.11*T0)
	A += 21 * math.Cos(5.340000+0.98*T0)
	A += 19 * math.Cos(1.850000+5486.78*T0)
	A += 19 * math.Cos(4.970000+213.30*T0)
	A += 17 * math.Cos(2.990000+6275.96*T0)
	A += 16 * math.Cos(0.030000+2544.31*T0)
	L = L + (A * T0)

	A = 8722 * math.Cos(1.0725+6283.0758*T0)
	A += 991 * math.Cos(3.1416)
	A += 295 * math.Cos(0.437+12566.1520*T0)
	A += 27 * math.Cos(0.050+3.52*T0)
	A += 16 * math.Cos(5.190+26.30*T0)
	A += 16 * math.Cos(3.69+155.42*T0)
	A += 9 * math.Cos(0.30+18849.23*T0)
	A += 9 * math.Cos(2.06+77713.77*T0)
	L = L + (A * common.Sqr(T0))

	A = 289 * math.Cos(5.842+6283.076*T0)
	A += 21 * math.Cos(6.05+12566.15*T0)
	A += 3 * math.Cos(5.20+155.42*T0)
	A += 3 * math.Cos(3.14)
	L = L + (A * common.Sqr(T0) * T0)
	L = L / 1.0e+8

	// {solar latitude}
	B = 280 * math.Cos(3.199+84334.662*T0)
	B += 102 * math.Cos(5.422+5507.553*T0)
	B += 80 * math.Cos(3.88+5223.69*T0)
	B += 44 * math.Cos(3.70+2352.87*T0)
	B += 32 * math.Cos(4.00+1577.34*T0)
	B = B / 1.0e+8

	A = 227778 * math.Cos(3.413766+6283.07585*T0)
	A += 3806 * math.Cos(3.3706+12566.1517*T0)
	A += 3620
	A += 72 * math.Cos(3.33+18849.23*T0)
	A += 8 * math.Cos(3.89+5507.55*T0)
	A += 8 * math.Cos(1.79+5223.69*T0)
	A += 6 * math.Cos(5.20+2352.87*T0)
	B = B + (A * T0 / 1.0e+8)

	A = 9721 * math.Cos(5.1519+6283.07585*T0)
	A += 233 * math.Cos(3.1416)
	A += 134 * math.Cos(0.644+12566.152*T0)
	A += 7 * math.Cos(1.07+18849.23*T0)
	B = B + (A * common.Sqr(T0) / 1.0e+8)

	A = 276 * math.Cos(0.595+6283.076*T0)
	A += 17 * math.Cos(3.14)
	A += 4 * math.Cos(0.12+12566.15*T0)
	B = B + (A * common.Sqr(T0) * T0 / 1.0e+8)

	var Result TStSunXYZRec

	// {solar radius vector (astronomical units)}
	Result.RV = 100013989
	Result.RV += 1670700 * math.Cos(3.0984635+6283.07585*T0)
	Result.RV += 13956 * math.Cos(3.05525+12566.15170*T0)
	Result.RV += 3084 * math.Cos(5.1985+77713.7715*T0)
	Result.RV += 1628 * math.Cos(1.1739+5753.3849*T0)
	Result.RV += 1576 * math.Cos(2.8649+7860.4194*T0)
	Result.RV += 925 * math.Cos(5.453+11506.770*T0)
	Result.RV += 542 * math.Cos(4.564+3930.210*T0)
	Result.RV += 472 * math.Cos(3.661+5884.927*T0)
	Result.RV += 346 * math.Cos(0.964+5507.553*T0)
	Result.RV += 329 * math.Cos(5.900+5223.694*T0)
	Result.RV += 307 * math.Cos(0.299+5573.143*T0)
	Result.RV += 243 * math.Cos(4.273+11790.629*T0)
	Result.RV += 212 * math.Cos(5.847+1577.344*T0)
	Result.RV += 186 * math.Cos(5.022+10977.079*T0)
	Result.RV += 175 * math.Cos(3.012+18849.228*T0)
	Result.RV += 110 * math.Cos(5.055+5486.778*T0)
	Result.RV += 98 * math.Cos(0.89+6069.78*T0)
	Result.RV += 86 * math.Cos(5.69+15720.84*T0)
	Result.RV += 86 * math.Cos(1.27+161000.69*T0)
	Result.RV += 65 * math.Cos(0.27+17260.15*T0)
	Result.RV += 63 * math.Cos(0.92+529.69*T0)
	Result.RV += 57 * math.Cos(2.01+83996.85*T0)
	Result.RV += 56 * math.Cos(5.24+71430.70*T0)
	Result.RV += 49 * math.Cos(3.25+2544.31*T0)
	Result.RV += 47 * math.Cos(2.58+775.52*T0)
	Result.RV += 45 * math.Cos(5.54+9437.76*T0)
	Result.RV += 43 * math.Cos(6.01+6275.96*T0)
	Result.RV += 39 * math.Cos(5.36+4694.00*T0)
	Result.RV += 38 * math.Cos(2.39+8827.39*T0)
	Result.RV += 37 * math.Cos(0.83+19651.05*T0)
	Result.RV += 37 * math.Cos(4.90+12139.55*T0)
	Result.RV += 36 * math.Cos(1.67+12036.46*T0)
	Result.RV += 35 * math.Cos(1.84+2942.46*T0)
	Result.RV += 33 * math.Cos(0.24+7084.90*T0)
	Result.RV += 32 * math.Cos(0.18+5088.63*T0)
	Result.RV += 32 * math.Cos(1.78+398.15*T0)
	Result.RV += 28 * math.Cos(1.21+6286.60*T0)
	Result.RV += 28 * math.Cos(1.90+6279.55*T0)
	Result.RV += 26 * math.Cos(4.59+10447.39*T0)
	Result.RV = Result.RV / 1.0e+8

	A = 103019 * math.Cos(1.107490+6283.075850*T0)
	A += 1721 * math.Cos(1.0644+12566.1517*T0)
	A += 702 * math.Cos(3.142)
	A += 32 * math.Cos(1.02+18849.23*T0)
	A += 31 * math.Cos(2.84+5507.55*T0)
	A += 25 * math.Cos(1.32+5223.69*T0)
	A += 18 * math.Cos(1.42+1577.34*T0)
	A += 10 * math.Cos(5.91+10977.08*T0)
	A += 9 * math.Cos(1.42+6275.96*T0)
	A += 9 * math.Cos(0.27+5486.78*T0)
	Result.RV = Result.RV + (A * T0 / 1.0e+8)

	A = 4359 * math.Cos(5.7846+6283.0758*T0)
	A += 124 * math.Cos(5.579+12566.152*T0)
	A += 12 * math.Cos(3.14)
	A += 9 * math.Cos(3.63+77713.77*T0)
	A += 6 * math.Cos(1.87+5573.14*T0)
	A += 3 * math.Cos(5.47+18849.23*T0)
	Result.RV = Result.RV + (A * common.Sqr(T0) / 1.0e+8)

	L = (L + math.Pi)
	L = Frac(L/2.0/math.Pi) * 2.0 * math.Pi
	if L < 0 {
		L = L + (2.0 * math.Pi)
	}
	B = -B

	Result.SLong = L * radcor
	Result.SLat = B * radcor

	X = Result.RV * math.Cos(B) * math.Cos(L)
	Y = Result.RV * math.Cos(B) * math.Sin(L)
	Z = Result.RV * math.Sin(B)

	Result.SunX = X + 4.40360e-7*Y - 1.90919e-7*Z
	Result.SunY = -4.79966e-7*X + 0.917482137087*Y - 0.397776982902*Z
	Result.SunZ = 0.397776982902*Y + 0.917482137087*Z
	return Result
} // SunPosPrim

// SunPos {-compute the RA/Declination of the Sun}
func SunPos(UT stdate.TStDateTimeRec) TStPosRec {
	var (
		SP     TStSunXYZRec
		Result TStPosRec
	)
	if !CheckDate(UT) {
		Result.RA = -1
		Result.DC = -99
		return Result
	}
	SP = SunPosPrim(UT)
	Result.RA = math.Atan2(SP.SunX, SP.SunY) * radcor
	Result.DC = math.Asin(SP.SunZ/SP.RV) * radcor
	return Result
}

// SunRiseSet {-compute the Sun rise or set time}
// {the value for H0 accounts for approximate refraction of 0.5667 deg. and}
// {that rise or set is based on the upper limb instead of the center of the solar disc}
func SunRiseSet(LD stdate.TStDate, Longitude, Latitude float64) TStRiseSetRec {
	var (
		// {the value for H0 accounts for approximate refraction of 0.5667 deg. and}
		// {that rise or set is based on the upper limb instead of the center of the solar disc}
		H0     float64
		UT     stdate.TStDateTimeRec
		RP     TStPosRecArray
		Result TStRiseSetRec
	)
	LD--
	H0 = -0.8333
	UT.T = 0
	UT.D = LD - 1
	if CheckDate(UT) {
		UT.D = UT.D + 2
		if !CheckDate(UT) {
			Result.ORise = -4
			Result.OSet = -4
			return Result
		}
		UT.D = UT.D - 2
	} else {
		Result.ORise = -4
		Result.OSet = -4
		return Result
	}

	for i := 0; i < 3; i++ {
		RP[i] = SunPos(UT)
		if i >= 2 {
			if RP[i].RA < RP[i-1].RA {
				RP[i].RA = RP[i].RA + 360
			}
		}
		UT.D++
	}
	Result = _RiseSetPrim(LD, Longitude, Latitude, H0, RP, false)
	return Result
} // SunRiseSet

// AmountOfSunlight {-compute the hours, min, sec of sunlight on a given date}
func AmountOfSunlight(LD stdate.TStDate, Longitude, Latitude float64) stdate.TStTime {
	var (
		RS     TStRiseSetRec
		Result stdate.TStTime
	)
	RS = SunRiseSet(LD, Longitude, Latitude)
	if RS.ORise == -3 {
		// {sun is always above the horizon}
		Result = stdate.SecondsInDay
		return Result
	}

	if RS.ORise == -2 {
		// {sun is always below horizon}
		Result = 0
		return Result
	}

	if RS.ORise > -1 {
		if RS.OSet > -1 {
			Result = RS.OSet - RS.ORise
		} else {
			Result = stdate.SecondsInDay - RS.ORise
		}
	} else {
		if RS.OSet > -1 {
			Result = RS.OSet
		} else {
			Result = 0
		}
	}
	if Result < 0 {
		Result = Result + stdate.SecondsInDay
	} else if Result >= stdate.SecondsInDay {
		Result = Result - stdate.SecondsInDay
	}
	return Result
} // AmountOfSunlight
