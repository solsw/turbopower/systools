package stastro

import (
	"testing"

	"github.com/solsw/timehelper"
	"gitlab.com/solsw/TurboPower/SysTools/stdate"
)

const (
	// https://geohack.toolforge.org/geohack.php?pagename=Yekaterinburg&params=56_50_08_N_60_36_46_E_type:city(1349772)_region:RU
	YekaterinburgLatitude  = 56.835556
	YekaterinburgLongitude = 60.612778
)

func TestAmountOfSunlight(t *testing.T) {
	type args struct {
		LD        stdate.TStDate
		Longitude float64
		Latitude  float64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "1",
			args: args{
				LD:        stdate.TimeToTStDate(timehelper.DateYMD(2021, 1, 16)),
				Longitude: YekaterinburgLongitude, Latitude: YekaterinburgLatitude},
			want: "9h31m37s"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := stdate.TStTimeToDuration(AmountOfSunlight(tt.args.LD, tt.args.Longitude, tt.args.Latitude)).String()
			if got != tt.want {
				t.Errorf("AmountOfSunlight() = %v, want %v", got, tt.want)
			}
		})
	}
}
