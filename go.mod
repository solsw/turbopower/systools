module gitlab.com/solsw/TurboPower/SysTools

go 1.15

require (
	github.com/solsw/mathrandhelper v0.1.0
	github.com/solsw/timehelper v0.1.0
	gitlab.com/solsw/delphi v0.1.0
)
